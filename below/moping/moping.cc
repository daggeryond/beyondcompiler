// below/moping/moping.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 28, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This file collects all moping algorithms
//
// GNU C++17
//
#include "moping.hh"


static
moper_container_t moper_container;

const moper_container_t &   Below::Moping::
available_mopers()
{
     return moper_container;
}

//
// List of Implementations
//

#include "triv_moper.hh"
moper_container["trivial"] = &Below::Moping::dots_triv_moper;


// EOF
