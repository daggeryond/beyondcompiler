// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created May 25, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// below/below_parser.y
//
// This is the Bison Grammar File for the low-level intermediate representation, ``Below''.
// This is genuine GNU Bison, incompatible with Yacc.
//
%require "3.0.4"
%define api.prefix {Below__yy}
%locations

%code requires {

#include "below_lang.hh"
#include "../base/all_parsers.hh"

     typedef All_Parsers::YYLTYPE YYLTYPE;

     struct QubitLogAddr
     {
          Base::LQubit_t lqubit                       = Base::Void_LQubit;
          Base::Addr_t   address                      = Base::Void_Addr;
     };

}


%parse-param { const char * const file_name };
%initial-action
{
     @$.first_line   = 1;
     @$.first_column = 1;
     @$.last_line    = 1;
     @$.last_column  = 1;
     @$.filename     = file_name;
};

%union {
     int                          nneg_int;
     const char *                 string;
     char                       * str;
     double                       dbl;

     Base::CReg_t                               creg;
     Base::CReg_with_Idx_t                      creg_with_idx;

     Base::Addr_t                               addr;
     QubitLogAddr                               qubit;
     Below::Below_Prg_Builder::Parse_Context *  p_context;
     Below::Operation                        *  p_operation;
}

%start below_prg
%type  <string>                       hw_id
%type  <p_context>                    qube_header
%type  <qubit>                        qubit
%type  <nneg_int>                     creg
%type  <nneg_int>                     bit_const
%type  <address>                      address
%type  <p_operation>                  operation

%destructor { the_below_prg->emergency_pop_context($$); } explicit_qube_header
%destructor { the_below_prg->emergency_pop_context($$); } begin_main


%token <nneg_int>                     T_nneg_int
%token <string>                       T_string
%token <string>                       T_cplx_id
%token <dbl>                          T_angle
%token <nneg_int>                     T_log_qubit
%token <nneg_int>                     T_creg

%token                                T_Address_Prefix '@'

%token                                T_begin_header "begin_header"
%token                                T_end_header   "end_header"
%token                                T_nqubits
%token                                T_ncregs
%token                                T_creg_sizes
%token                                T_hardware_id

%token                                T_begin_qubes
%token                                T_end_qubes
%token                                T_Qube
%token                                T_Opaque
%token                                T_time
%token                                T_data_in
%token                                T_borrows_in
%token                                T_data
%token                                T_borrows
%token                                T_zeros

%token                                T_begin_main
%token                                T_end_main
%token                                T_qalloc
%token                                T_qfree
%token                                T_cU3
%token                                T_SWAP
%token                                T_U3
%token                                T_exchange
%token                                T_measure
%token                                T_reset
%token                                T_rightarrow "->" // == "->" (measurement target register)
%token                                T_eqeq       "==" // == "==" (comparison)

%token                                T_loop
%token                                T_break_if
%token                                T_cont_if

// Old-style loops
%token                                T_if
%token                                T_while

%token END 0 "end of file"

%code {
     static Below::Below_Prg_Builder * the_below_prg = new Below::Below_Prg_Builder{file_name};
}


//                       Bison Grammar Rules
%%

below_prg : header qubes main { $$ = the_below_prg; }
          ;

////////////////////////////////////////////////////////////////////////////////////////////////////

header      :   T_begin_header nqubits creg_decl hw_id T_end_header
            ;

nqubits     :   T_nqubits T_nneg_int
                                                        {
                                                             the_below_prg->set_nqubits(@T_nneg_int,$T_nneg_int);
                                                        }
            ;

creg_decl   :   T_ncregs  T_nneg_int
                                                        {    // Mid-rule action (Safe: nothing has to be undone in case of parse error)
                                                             the_below_prg->set_ncregs(@T_nneg_int,$T_nneg_int);
                                                        }
                T_creg_sizes '{' creg_sz_lst '}'[close]
                                                        {
                                                             the_below_prg->finish_cregs(@close);
                                                        }
            ;


creg_sz_lst :   %empty
            |   sizes
            ;

sizes       :   T_nneg_int                                {    the_below_prg->append_crgsz(@$,$T_nneg_int); }
            |   creg_sz_lst ',' T_nneg_int                {    the_below_prg->append_crgsz(@T_nneg_int,$T_nneg_int); }
            ;

hw_id       :   T_hardware_id  T_string                   {    the_below_prg->set_backend(@T_string,$T_string);   }
            ;

////////////////////////////////////////////////////////////////////////////////////////////////////

qubes                :      %empty
                     |      T_begin_qubes qube_def_list T_end_qubes
                     ;



qube_def_list        :      %empty
                     |      qube_def_list qube_def
                     ;



qube_def             :     opaque_qube_header
                     |     explicit_qube_header operation_seq '}'
                                                                     {
                                                                          the_below_prg->pop_context(@explicit_qube_header, $explicit_qube_header);
                                                                     }
                     ;


opaque_qube_header   :     qube_header ';'
                     ;


explicit_qube_header :     qube_header '{'                           //   NEEDS  %destructor
                                                                     {
                                                                          $$ = the_below_prg->push_context(@qube_header,$qube_header);
                                                                     }
                     ;



qube_header          :     T_Qube T_cplx_id '[' T_id ']' '(' T_nneg_int[width] ',' T_nneg_int[height] ')'
                           T_data_in  '(' qubit_list[input_qubit_args]  ')'   T_borrows_in  '(' qubit_list[input_borrowed_qubits]  ')'   T_zeros '(' qubit_list[input_ancilla_qubits] ')'
                           T_data_out '(' qubit_list[output_qubit_args] ')'   T_borrows_out '(' qubit_list[output_borrowed_qubits] ')'   // set of zeros is defined by data+borrows+forbidden
                                                                     {
                                                                          Below::Qube *p_qb = new Below::Qube{};
                                                                          p_qb->
                                                                          
                                                                     }
                     ;

////////////////////////////////////////////////////////////////////////////////////////////////////

main         :     begin_main operations_list T_end_main
                                                                     {
                                                                          the_below_prg->pop_context(@T_end_main, $begin_main);
                                                                     }
             ;



begin_main   :     T_begin_main
                                                                     {    // NEEDS  %destructor
                                                                          $$ = the_below_prg->push_context(@T_begin_main, Below::Below_Prg_Builder::Main_Context);
                                                                     }

////////////////////////////////////////////////////////////////////////////////////////////////////



operation_seq    :    %empty
                 |    operations_list operation[oper]                {    $$ = std::move($lhs.push_back($rhs));    }
                 ;



operation        : T_U3 '(' T_angle[phi] ',' T_angle[lambda] ',' T_angle[theta] ')' qubit[qarg] ';'
                                               {
                                                    $$ = std::unique_ptr<Operation>{new U3_Operation{$phi, $lambda, $theta, $qarg}};
                                               }
                 | T_cU3 '(' T_angle[phi] ',' T_angle[lambda] ',' T_angle[theta] ')' qubit[qarg_ctrl] qubit[qarg_targ] ';'
                                               {
                                                    $$ = std::unique_ptr<Operation>{new Controlled_U3_Operation{$phi, $lambda, $theta, $qarg_ctrl, $qarg_targ}};
                                               }
                 | T_SWAP qubit[qarg1] qubit[qarg2] ';'
                                               {
                                                    $$ = std::unique_ptr<Operation>{new Swap_Operation{$qarg1, $qarg2}};
                                               }
                 | T_Qube T_qube_name[qube_name] '(' qubit_list[qargs] ')' ';'
                                               {
                                                    $$ = std::unique_ptr<Operation>{new Qube_Call{$qube_name, $qargs}};
                                               }
                 | T_measure qubit[qarg] T_rightarrow cbit[result_bit] ';'
                                               {
                                                    $$ = std::unique_ptr<Operation>{new Measurement_Operation{$qarg, $result_bit}};
                                               }
                 | '{' operations_list[op_list] '}'
                                               {
                                                    $$ = std::unique_ptr<Operation>{new Operation_Sequence{$op_list}};
                                               }
                 | T_if '(' cbit[cbit_idx] T_eqeq bit_const[cval] ')' operation[op]
                                               {
                                                    $$ = std::unique_ptr<Operation>{new If_Operation{$cbit_idx, $cval, $op}};
                                               }
                 | T_while '<' T_nneg_int[max_it_bound] '>' '(' cbit[cbit_idx] T_eqeq bit_const[cval] ')' operation[op]
                                               {
                                                    $$ = std::unique_ptr<Operation>{new While_Operation{$max_it_bound, $cbit_idx, $cval, $op}};
                                               }
                 | T_qalloc qubit[qarg] ';'    {
                                                    $$ = std::unique_ptr<Operation>{new Qalloc_Operation{$qarg}};
                                               }
                 | T_qfree qubit[qarg] ';'     {
                                                    $$ = std::unique_ptr<Operation>{new Qfree_Operation{$qarg}};
                                               }
                 | T_exchange '(' qubit[qarg0] ',' qubit[qarg1] ')' ';'
                                               {
                                                    $$ = std::unique_ptr<Operation>{new Exchange_Operation{$qarg0, $qarg1}};
                                               }
                 ;


qubit_list   :   qubit[qbit_idx]                   {   $$ = Below::Qubit_List_t(); $$.push_back($qbit_idx);  }
             |   qubit_list[lhs] ',' qubit[rhs]    {   $$ = std::move($lhs.push_back($rhs));          }
             ;


qubit        :   T_log_qubit[qubit_idx] address[addr]
                                                   {
                                                        $$ = Below::Qubit($qubit_idx, $addr);
                                                   }
             ;


creg         :   T_creg[reg_idx]              {    $$ = $rtb_idx;              }
             ;


bit_const    :   T_nneg_int[val]                     {    $$ = $val;                  }
             ;


address         :    %empty                        {
                                                        $$ = Below::Address(-1, -1);
                                                   }
                |    ':' ADDRESS_PREFIX '(' T_nneg_int[x] ',' T_nneg_int[y] ')'
                                                   {
                                                        $$ = Below::Address($x, $y);
                                                   }
                ;
%%
