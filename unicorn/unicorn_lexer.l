
/*
  Start Conditions

  Enable lexing of complex IDs (those including >.<)
  just before the name of a unitary or procedure is expected.
  Once the complex ID is lexed, the start condition is disabled
  using BEGIN(0)

  https://westes.github.io/flex/manual/Start-Conditions.html#Start-Conditions
*/
%s ExpectCplxID


HEX_DIGITS       0[xX][[:digit:][A-F]]+
BINARY_DIGITS    0[bB][01]+
FLOAT            ([+-]?(\.[:digit]+|[:digit:]+\.[:digit:]*)([eE][+-]?[:digit:]+)?)|([+-]?([:digit:]+)[eE][+-]?[:digit:]+)
XALPHA           [[:alpha:]_]

"APPLY"                                                                    {    BEGIN(ExpectCplxID);
                                                                                return T_Apply;
                                                                           }

"UNITARY"                                                                  {    BEGIN(ExpectCplxID);
                                                                                return T_Unitary;
                                                                           }


<expect_cplx_IDs>XALPHA[[:alnum:][._<>]]*                                  {    BEGIN(0);
                                                                                ...
                                                                                return T_cplx_id;
                                                                           }
