// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created May 25, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// below/below_lang.cc
//
// This file contains the definitions of the Below language.
//
// GNU C++17
//
#include "below_lang.hh"

#include <typeinfo>
#include <sstream>


// class  Connectivity
// -------------------
template<Connectivity C>
inline
bool
Below::Connectivity::
is_legal(XY_t xy, XY_t uv) const
{
     if (  std::abs(xy.x-uv.x) > 1   ||   std::abs(xy.y-uv.y) > 1  )  return false;
     if ( xy.x==uv.x || xy.y==uv.y ) return true;
#    ifdef BEYOND__NODEBUG
     if ( xy.x==uv.x && xy.y==uv.y ) throw std::runtime_error("Below::Connectivity::is_legal(): It seems like there's an operation on two operands, but the two operands have the same address.");
#    endif
     switch(C) {  // g++ will optimize this away (I hope)
     case grid_O:
          return false;
     case chess_XO:
          return ( std::min(xy.x,uv.x) + std::min(xy.y,uv.y) )%2  ==0;
     case chess_OX:
          return ( std::min(xy.x,uv.x) + std::min(xy.y,uv.y) )%2  ==1;
     case grid_X:
          return true;
     }
}//^ Connectivity:: is_legal()


// class  Qube
// -----------
bool
Below::Qube::
same_trafo(const Qube & _Q) const
{
     if (m_name==_Q.name) {
          if (m_num_lqubits == _Q.m_num_lqubits) return true;
          std::string msg = "Two qubes with name ''"+m_name+"'' have different number of logical input/output qubits.\n";
          throw Below::compile_error(msg, m_source_loc, _Q.m_source_loc);
     }
     return false;
}//^ Qube:: same_trafo()


bool
Below::Qube::Qube_Context::
is_legal(const Operation & op, std::string * const p_errmsg) const
{
     if (op.type()== //TODO: Complete this or delete this

     if ( op.has_q() ) {
          const XY_t q1 = op.q1();
          if ( ! Q.dims().contains(q1) )                                       return *p_errmsg="Operand out of bounds.", false;
          if ( Q.m_layout[Q.m_dims.linearize(q1)]==Qube::Addr_Tag::forbidden ) return *p_errmsg="Qube operation's operand is in forbidden area of qube.", false;

          if ( op.has_2nd_q() ) {
               if ( ! Q.dims().contains(op.q2()) )                                  return *p_errmsg="Second operand out of bounds.", false;
               if ( Q.m_layout[Q.m_dims.linearize(q1)]==Qube::Addr_Tag::forbidden ) return *p_errmsg="Qube operation's second operand is in forbidden area of qube.", false;

               if ( ! Q.m_connectivity.is_legal(q1,q2) )                            return *p_errmsg="Qube operands not coupled (connecitivy).", false;
          }
     }
     return true;
}//^ Qube_Context:: is_legal()


namespace Below {

     // Helper functions
     // ----------------
     // inline templates

     template<typename T>
     std::string
     to_json(const std::vector< T* > & Tp_list)
     {
         std::ostringstream ss;
         bool first = true;
         for (const auto & item : Tp_list) {
             if(first) {
                 first = false;
             } else {
                 ss << ", ";
             }
             ss << item->to_json();
         }
         std::string json_string = ss.str();
         return json_string;
     }//^ to_json()

     template<typename T>
     std::string
     to_json(const std::vector< T > & T_list)
     {
          std::ostringstream ss;
          bool first = true;
          for (const auto & item : T_list) {
               if(first) {
                    first = false;
               } else {
                    ss << ", ";
               }
               ss << item.to_json();
          }
          std::string json_string = ss.str();
          return json_string;
     }//^ to_json()

     std::string
     qubes_to_json(const std::vector< Qube > & qube_list)
     {
          std::ostringstream ss;
          bool first = true;
          for (const auto & qube : qube_list) {
               if(first) {
                    first = false;
               } else {
                    ss << ", ";
               }
               ss << ("\"" + qube.name() + "\": " + qube.to_json());
          }
          std::string json_string = ss.str();
          return json_string;
     }//^ qubes_to_json()

     template<typename T>
     std::string
     to_below_code(const std::vector< T* > & Tp_list, const std::string separator)
     {
         std::ostringstream ss;
         bool first = true;
         for (const auto & item : Tp_list) {
             if(first) {
                 first = false;
             } else {
                 ss << separator;
             }
             ss << item->to_below_code();
         }
         std::string below_string = ss.str();
         return below_string;
     }//^ to_below_code()

     template<typename T>
     std::string
     to_below_code(const std::vector< T > & T_list, const std::string separator)
     {
          std::ostringstream ss;
          bool first = true;
          for (const auto & item : T_list) {
               if(first) {
                    first = false;
               } else {
                    ss << separator;
               }
               ss << item.to_below_code();
          }
          std::string below_string = ss.str();
          return below_string;
     }//^ to_below_code()
     
     std::string
     addresses_to_json(std::vector<XY_t>& addresses) {
         std::ostringstream ss;
         bool first = true;
         for (const XY_t & address : addresses) {
              if(first) {
                   first = false;
              } else {
                   ss << ", ";
              }
              ss << "{\"logical\": -1, \"physical\": {\"x\": " + std::to_string(address.x) + ", \"y\": " + std::to_string(address.y) + "}}";
         }
         std::string json_string = ss.str();
         return json_string;
     }
     
     std::string
     addresses_to_below_code(std::vector<XY_t>& addresses) {
         std::ostringstream ss;
         bool first = true;
         for (const XY_t & address : addresses) {
              if(first) {
                   first = false;
              } else {
                   ss << ", ";
              }
              ss << "p#(" + std::to_string(address.x) + "," + std::to_string(address.y) + ")";
         }
         std::string below_string = ss.str();
         return below_string;
     }
     
     std::string
     qubit_to_below_code(LQubit_t lqubit, Addr_t address) {
         return "q#" + std::to_string(lqubit) + ":p#(" + std::to_string(address.x) + "," + std::to_string(address.y) + ")";
     }
     
     std::string
     fridgebit_to_below_code(CReg_with_Idx_t creg_w_idx) {
         return "(classical bit)"; //TODO: Implement correctly
     }
     
     std::string
     qubit_to_json(LQubit_t lqubit, Addr_t address) {
         return "{\"logical\": " + std::to_string(lqubit) + ", \"physical\": {\"x\": " + std::to_string(address.x) + ", \"y\": " + std::to_string(address.y) + "}}";
     }
     
     std::string
     fridgebit_to_json(CReg_with_Idx_t creg_w_idx) {
         return "\"classical bit\""; //TODO: Implement correctly
     }
     
     std::vector<XY_t>
     Qube::get_inputAddr_of_role(Role_tag role) const
     {
          std::vector<XY_t> filtered_addrs;
          for(XY_t const &crr_addr : inputAddr_of_qubit) {
              if(input_role.at(crr_addr.lin()) == role))
                  filtered_addrs.push_back(crr_addr);
          }
          return filtered_addrs;
     }
     
     std::vector<XY_t>
     Qube::get_outputAddr_of_role(Role_tag role) const
     {
          std::vector<XY_t> filtered_addrs;
          for(XY_t const &crr_addr : outputAddr_of_qubit) {
              if(output_role.at(crr_addr.lin()) == role))
                  filtered_addrs.push_back(crr_addr);
          }
          return filtered_addrs;
     }
     
     std::string
     Qube::to_json() const
     {
         std::string json_string = "{\"name\": \"" + name() + "\", \"datebase_identifier\": \"" + database_id + "\", ";
         if (opague)
              json_string += "\"time_bound\": " + std::to_string(time_bound) + ", ";
         json_string += "\"width\": " + std::to_string(dims.x) + ", ";
         json_string += "\", \"height\": " + std::to_string(dims.y);
         json_string += ", \"input_qargs\": [" + addresses_to_json(get_InputAddr_of_role(Role_tag.data));
         json_string += "], \"input_ancilla_qargs\": [" + addresses_to_json(get_InputAddr_of_role(Role_tag.zero)); 
         json_string += "], \"input_borrowed_qargs\": [" + addresses_to_json(get_InputAddr_of_role(Role_tag.borrowed));
         json_string += "], \"output_qargs\": [" + addresses_to_json(get_OutputAddr_of_role(Role_tag.data));
         json_string += "], \"output_ancilla_qargs\": [" + addresses_to_json(get_OutputAddr_of_role(Role_tag.zero));
         json_string += "], \"output_borrowed_qargs\": [" + addresses_to_json(get_OutputAddr_of_role(Role_tag.borrowed)) + "]";
         if (not opague)
             json_string += ", \"body\": [" + Below::to_json(op_seq) + "]";
         json_string += "}";
         return json_string;
     }
     
     std::string
     Qube::to_below_code() const
     {
         std::string below_string = ""
         if(opague) {
             below_string += "  Opague Qube " + database_id + " (time = " + std::to_string(time()) + ") "
         else {
             below_string += "  Qube " + database_id + " "
         }
         below_string += name() + "[" + std::to_string(dims.x) + "," + std::to_string(dims.y) + "]\n";
         below_string += "    input data = (" + addresses_to_below_code(get_InputAddr_of_role(Role_tag.data)) + ")\n";
         below_string += "    input zero = (" + addresses_to_below_code(get_InputAddr_of_role(Role_tag.zero)) + ")\n";
         below_string += "    input borrowed = (" + addresses_to_below_code(get_InputAddr_of_role(Role_tag.borrowed)) + ")\n";
         below_string += "    output data = (" + addresses_to_below_code(get_OutputAddr_of_role(Role_tag.data)) + ")\n";
         below_string += "    output zero = (" + addresses_to_below_code(get_OutputAddr_of_role(Role_tag.zero)) + ")\n";
         below_string += "    output borrowed = (" + addresses_to_below_code(get_OutputAddr_of_role(Role_tag.borrowed)) + ")\n";
         if(not opague) {
             below_string += "  {\n    "
             below_string += Below::to_below_code(op_seq, "    ")
             below_string += "  }\n"
         } else {
             below_string += "  ;\n"
         }
         return below_string;
     }
     
     std::string 
     Qube_Call::to_json() {
         return "\"Qube_Call\""; //TODO: Implement correctly
     }
     
     std::string
     Qube_Call::to_below_code() {
         return "Qube_Call"; //TODO: Implement correctly
     }
     
     std::string 
     Measurement::to_json() {
         return "{\"sort\": \"measure\", \"qubit\": " + qubit_to_below_code(lqubit, address) + ", \"cbit\":" + fridgebit_to_below_code(creg_w_idx) + "}";
     }
     
     std::string
     Measurement::to_below_code() {
         return "measure " + qubit_to_below_code(lqubit, address) + " -> " + fridgebit_to_below_code(creg_w_idx) + ";";
     }
     
     std::string 
     One_Qubit_Unitary::to_json() {
         return "\"One_Qubit_Unitary\""; //TODO: Implement correctly
     }
     
     std::string
     One_Qubit_Unitary::to_below_code() {
         return "One_Qubit_Unitary"; //TODO: Implement correctly
     }
     
     std::string 
     Two_Qubit_Unitary::to_json() {
         return "\"Two_Qubit_Unitary\""; //TODO: Implement correctly
     }
     
     std::string
     Two_Qubit_Unitary::to_below_code() {
         return "Two_Qubit_Unitary"; //TODO: Implement correctly
     }
     
     std::string 
     Loop_Operation::to_json() {
         return "\"Loop_Operation\""; //TODO: Implement correctly
     }
     
     std::string
     Loop_Operation::to_below_code() {
         return "Loop_Operation"; //TODO: Implement correctly
     }
     
     std::string 
     IfThenElse::to_json() {
         return "\"IfThenElse\""; //TODO: Implement correctly
     }
     
     std::string
     IfThenElse::to_below_code() {
         return "IfThenElse"; //TODO: Implement correctly
     }

     std::string
     Below_Prg::to_json() {
         header_json = "\"header\""; //TODO: Implement correctly
         qubes_json = "{";
         bool is_first = true; 
         for(const auto name_qube_pair : the_qubes) {
             if(is_first) {
                 is_first = false;
             } else {
                 qubes_json += ", ";
             }
             std::string qube_name = name_qube_pair.first;
             Qube* qube = name_qube_pair.second;
             qubes_json += "\"" + qube_name + "\": " + (*qube).to_below_code();
         }
         qubes_json += "}";
         main_json = "\"main\""; //TODO: Implement correctly
         return "{\"header\": " + header_json + ", \"qubes\": " + qubes_json + ", \"main\": " + main_json + "}";
     }
     
     std::string
     Below_Prg::to_below_code() {
         std::string header_below_code = "begin_header\n  nqubits " + std::to_string(n_lqubits);
         header_below_code += "  [fridge bit register sizes]\n"; //TODO: Implement correctly
         header_below_code += "  hardware_id \"" + get_backend().id + "\"\nend_header\n\n";
         std::string qubes_below_code = "begin_qubes\n";
         for(const auto name_qube_pair : the_qubes) {
             Qube* qube = name_qube_pair.second;
             qubes_below_code += "  " + (*qube).to_below_code() + "\n";
         }
         qubes_below_code += "end_qubes\n\n";
         std::string main_below_code = "begin_main\n";
         for(const Operation* operation : the_main) {
             main_below_code += "  " + operation.to_below_code() + "\n";
         }
         main_below_code += "end_main\n\n";
         return header_below_code + qubes_below_code + main_below_code;
     }
     
}//^ namespace Below

//EOF
