// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 20, 2018 by TVC
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// unicorn/unicorn_lang.hh
//
// This file contains the definitions of the Unicorn language.
//
// GNU C++17
//
#ifndef __UNICORN_UNICORN_LANG_HH__
#define __UNICORN_UNICORN_LANG_HH__

#include "../base/base.hh"
#include "../traversary/traversary.hh"

namespace Unicorn {
    
    class Unicorn_Header {
        //TODO: Fill in stuff
    public:
        inline
        Unicorn_Header() {}
        
        ~Unicorn_Header();
    }
    
    class Unicorn_Statement {
        
    public:
        virtual bool is_unitary() = 0;
    }
    
    class Loop_Statement : public Unicorn_Statement {
        std::vector<Unicorn_Statement*> body;
        unsigned n_iter;
        Traversary::Highlevel_Controlflow_Condition *entry_condition;
        Traversary::Highlevel_Controlflow_Condition *exit_condition;
    public:
        inline
        Loop_Statement(
            std::vector<Unicorn_Statement*> body_,
            unsigned n_iter_, Traversary::Highlevel_Controlflow_Condition *entry_condition_, Traversary::Highlevel_Controlflow_Condition *exit_condition_
        ) body{body_} n_iter{n_iter_} entry_condition{entry_condition_} exit_condition{exit_condition} {}
        
        ~Loop_Statement();
        
        bool is_unitary();
    }
    
    class If_Then_Else_Statement : public Unicorn_Statement {
        Traversary::Highlevel_Controlflow_Condition *condition;
        std::vector<Unicorn_Statement*> then_body;
        std::vector<Unicorn_Statement*> else_body;
    public:
        If_Then_Else_Statement(
            Traversary::Highlevel_Controlflow_Condition *condition_, std::vector<Unicorn_Statement*> then_body_, std::vector<Unicorn_Statement*> else_body_
        ) condition{condition_} then_body{then_body_} else_body{else_body_} {}
        
        ~If_Then_Else_Statement();
        
        inline
        bool is_unitary();
    }
    
    class Proc_Definition;
    
    class Proc_Call : public Unicorn_Statement {
        Proc_Definition *called_operation;
        
        bool validate();
        
        inline
        bool is_unitary() {
            return false;
        }
        
        ~Proc_Call();
    }
    
    class Unitary_Defintion;
    
    class Unitary_Call : public Unicorn_Statement {
        bool dagger;
        bool ctrled;
        LQubit_t ctrling_qubit;
        Proc_Definition *called_operation;
        
        bool validate();
        
        inline
        bool is_unitary() {
            return false;
        }
        
        ~Unitary_Call();
    }
    
    class Elementary_Unicorn_Operation : public Unicorn_Statement {
        bool unitary;
        unique_ptr<Traversary::Highlevel_Elementary_Operation> operation;
        vector<LQubit_t> operands;
        
        inline
        bool is_unitary() {
            return unitary;
        }
    }
    
    class Proc_Definition {
        std::vector<Unicorn_Statement> code;
        
        inline
        bool is_unitary() {
            return false;
        }
        
        bool validate();
    }
    
    class Unitary_Definition {
        std::vector<Unicorn_Statement> code;
        
        inline
        bool is_unitary() {
            return true;
        }
        
        bool validate();
    }
    
    class Unicorn_Prg {
        Unicorn_Header header;
        std::vector<Proc_Definition> proc_definitions;
        
        bool validate();
    }
    
}//^ namespace Unicorn


#endif
//EOF
