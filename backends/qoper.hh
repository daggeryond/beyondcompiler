// backends/qoper.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created May 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This file contains the representations of fundamental quantum operations:
//   1,2 qubit qoper,
//   measurement.
//
// GNU C++17
//
#ifndef __QOPER_HH__
#define __QOPER_HH__

namespace Backends {

#warning "backends/qoper.hh: Todo: Implement other operations (currently only unitaries)."

     //
     // Unitaries
     // ---------
     //

     //
     // These are "physical" unitaries, meaning, defined up to global phase.
     //


     class Physical_Unitary_1
     {
     protected:
          mutable  float my_time                                                = -1.;
          // Todo: more cost measures will go here

     public:
          float time()         const                                            { return my_time; }
                                                                                // -1 indicates time has not been computed
          template<typename BACKEND>
          void  get_time()     const                                            { time=compute_time<BACKEND>(*this); }
     };


     class Physical_Productbasis_Unitary_2
     {
     protected:
          mutable float my_time                                                 = -1.;
          // Todo: more cost measures will go here

     public:

          float time()         const                                            { return my_time; }

          template<typename BACKEND>
          void  get_time()     const                                            { time=compute_time<BACKEND>(*this); }
     };



} //^ namespace

#endif //EOF
