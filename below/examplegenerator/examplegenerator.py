#!/usr/bin/env python

import random

class Vertex_Content:
    def to_gate(self):
        raise Exception("AbstractMethodError!")

class Filler_Based_Vertex_Content(Vertex_Content):
    def __init__(self, filler):
        self.filler = filler
    
    def to_gate(self):
        return self.filler.to_gate()

class Qube_Based_Vertex_Content(Vertex_Content):
    def __init__(self, qube):
        self.qube = qube
    
    def to_gate(self):
        return Qube_Gate(self.qube)

class Qalloc_Vertex_Content(Vertex_Content):
    def __init__(self, logical_qubit):
        self.logical_qubit = logical_qubit
    
    def to_gate(self):
        return Qalloc_Gate(self.logical_qubit)

class Qfree_Vertex_Content(Vertex_Content):
    def __init__(self, logical_qubit):
        self.logical_qubit = logical_qubit
    
    def to_gate(self):
        return Qfree_Gate(self.logical_qubit)

class Gate:
    def to_invocation_string(self):
        raise Exception("AbstractMethodError!")
    
    def to_solution_string(self):
        raise Exception("AbstractMethodError!")
    
    def is_u3(self):
        return False

class Qube_Gate(Gate):
    def __init__(self, qube):
        self.qube = qube
    
    def to_invocation_string(self):
        return self.qube.to_invocation_string()
    
    def to_solution_string(self):
        return self.qube.to_solution_string()

class Elementary_Operation:
    def get_invocation_string(self, string_for_qubits):
        raise Exception("AbstractMethodError!")
    
    def get_solution_string(self, string_for_qubits):
        raise Exception("AbstractMethodError!")

class U3_Operation:
    def __init__(self, ng_phi, ng_lambda, ng_theta):
        self.ng_phi = ng_phi
        self.ng_lambda = ng_lambda
        self.ng_theta = ng_theta
    
    def get_invocation_string(self, qubit_indices):
        return "  U3(" + str(self.ng_phi) + ", " + str(self.ng_lambda) + ", " + str(self.ng_theta) + ") q#" + str(qubit_indices[0]) + ";\n"
    
    def get_solution_string(self, qubit_indices, position_strings):
        return "  U3(" + str(self.ng_phi) + ", " + str(self.ng_lambda) + ", " + str(self.ng_theta) + ") q#" + str(qubit_indices[0]) + ":" + position_strings[0] + ";\n"
    
    def is_u3(self):
        return True

class Controlled_U3_Operation:
    def __init__(self, ng_phi, ng_lambda, ng_theta):
        self.ng_phi = ng_phi
        self.ng_lambda = ng_lambda
        self.ng_theta = ng_theta
    
    def get_invocation_string(self, qubit_indices):
        return "  cU3(" + str(self.ng_phi) + ", " + str(self.ng_lambda) + ", " + str(self.ng_theta) + ") q#" + str(qubit_indices[0]) + " q#" + str(qubit_indices[1]) + ";\n"
    
    def get_solution_string(self, qubit_indices, position_strings):
        result = "  cU3(" + str(self.ng_phi) + ", " + str(self.ng_lambda) + ", " + str(self.ng_theta) + ")"
        result += " q#" + str(qubit_indices[0]) + ":" + position_strings[0] + " q#" + str(qubit_indices[1]) + ":" + position_strings[1] + ";\n"
        return result

class Measurement_Operation:
    def __init__(self, classical_bit_index):
        self.classical_bit_index = classical_bit_index
    
    def get_invocation_string(self, qubit_indices):
        return "  measure q#" + str(qubit_indices[0]) + " -> c#" + str(self.classical_bit_index) + ";\n"
    
    def get_solution_string(self, qubit_indices, solution_strings):
        return "  measure q#" + str(qubit_indices[0]) + ":" + solution_strings[0] + " -> c#" + str(self.classical_bit_index) + ";\n"

def place_to_index(place):
    if place.assigned_logical_qubit != None:
        return place.assigned_logical_qubit.lq_index
    else:
        raise Exception("Shouldn't happen!")

class Elementary_Gate(Gate):
    def __init__(self, elem_operation, places):
        self.elem_operation = elem_operation
        self.places = places
    
    def to_invocation_string(self):
        qubit_indices = list(map(lambda place : place_to_index(place), self.places))
        return self.elem_operation.get_invocation_string(qubit_indices)
    
    def to_solution_string(self):
        qubit_indices = list(map(lambda place : place_to_index(place), self.places))
        solution_strings = list(map(lambda place : place.to_position_string(), self.places))
        return self.elem_operation.get_solution_string(qubit_indices, solution_strings)

class Qalloc_Gate(Gate):
    def __init__(self, logical_qubit):
        self.logical_qubit = logical_qubit
    
    def to_invocation_string(self):
        return "  qalloc q#" + str(self.logical_qubit.lq_index) + ";\n"
    
    def to_solution_string(self):
        pp = self.logical_qubit.start_station.next_station.get_input_position()
        return "  qalloc q#" + str(self.logical_qubit.lq_index) + ":p#(" + str(pp.x_coord) + "," + str(pp.y_coord) + ");\n"

class Qfree_Gate(Gate):
    def __init__(self, logical_qubit):
        self.logical_qubit = logical_qubit
    
    def to_invocation_string(self):
        return "  qfree q#" + str(self.logical_qubit.lq_index) + ";\n"
    
    def to_solution_string(self):
        pp = self.logical_qubit.end_station.prev_station.get_output_position()
        return "  qfree q#" + str(self.logical_qubit.lq_index) + ":p#(" + str(pp.x_coord) + "," + str(pp.y_coord) + ");\n"

class Filler:
    def __init__(self, inside_gate_index, gate_inst):
        self.inside_gate_index = inside_gate_index
        self.gate_inst = gate_inst
    
    def to_gate(self):
        if self.inside_gate_index != 0:
            raise Exception("Shouldn't happen!")
        return self.gate_inst
    
    def forbids_realloc_after(self):
        return self.gate_inst.is_u3()

def sample_angle():
    if random.randint(0,1) == 0:
        return random.uniform(0, 2 * 3.141592)
    else:
        return random.uniform(-3.141592, 3.141592)

def get_random_single_qubit_operation(place, ncbits, measurement_chance):
    r = random.uniform(0.0, 1.0)
    if r < measurement_chance:
        elem_operation = Measurement_Operation(random.randint(0, ncbits-1))
    else:
        elem_operation = U3_Operation(sample_angle(), sample_angle(), sample_angle())
    return Elementary_Gate(elem_operation, [place])

def get_random_single_qubit_measurement_operation(place, ncbits):
    elem_operation = Measurement_Operation(random.randint(0, ncbits-1))
    return Elementary_Gate(elem_operation, [place])

def get_random_two_qubit_operation(place1, place2):
    elem_operation = Controlled_U3_Operation(sample_angle(), sample_angle(), sample_angle())
    return Elementary_Gate(elem_operation, [place1, place2])

def get_corrected_chessboard_tag(x, y, grid_chessboard_tag): #0 = white-white 1 = white-black 2 = black-white 3 = black-black
    if grid_chessboard_tag == 0 or grid_chessboard_tag == 3:
        return grid_chessboard_tag
    else:
        if x + y % 2 == 0:
            return grid_chessboard_tag
        else:
            return 3 - grid_chessboard_tag

def get_random_neighbor(x_coord, y_coord, grid_width, grid_height, chessboard_tag):
    r = random.randint(0, 3)
    if r == 0:
        return None
    corrected_chess_board_tag = get_corrected_chessboard_tag(x_coord, y_coord, chessboard_tag)
    posibility_count = 6
    if corrected_chess_board_tag == 0:
        posibility_count = 4
    if corrected_chess_board_tag == 3:
        posibility_count = 8
    r = random.randint(0, posibility_count - 1)
    if corrected_chess_board_tag == 2 and r > 3:
        r += 2
    if r == 0:
        x_offset = 0
        y_offset = 1
    elif r == 1:
        x_offset = 0
        y_offset = -1
    elif r == 2:
        x_offset = 1
        y_offset = 0
    elif r == 3:
        x_offset = -1
        y_offset = 0
    elif r == 4:
        x_offset = -1
        y_offset = 1
    elif r == 5:
        x_offset = 1
        y_offset = -1
    elif r == 6:
        x_offset = 1
        y_offset = 1
    else: #r == 7
        x_offset = -1
        y_offset = -1
    neighbor_x = x_coord + x_offset
    neighbor_y = y_coord + y_offset
    if neighbor_x < 0 or neighbor_x >= grid_width:
        return None
    if neighbor_y < 0 or neighbor_y >= grid_height:
        return None
    return Spatial_Position(neighbor_x, neighbor_y)

def fill_layer_up_with_elem(layer, grid_width, grid_height, chessboard_tag, ncbits, measurement_chance, fillers):
    places_to_fill = []
    for row in layer:
        for place in row:
            if place.is_free():
                places_to_fill.append(place)
    random.shuffle(places_to_fill)
    for place in places_to_fill:
        if place.is_free():
            neighbor = None
            r = random.randint(0, 3)
            if r != 0:
                it_count = 0
                while neighbor == None and it_count < 3:
                    neighbor = get_random_neighbor(place.x, place.y, grid_width, grid_height, chessboard_tag)
                    if neighbor != None:
                        neighbor_place = layer[neighbor.y_coord][neighbor.x_coord]
                        if (not neighbor_place.is_free()):
                            neighbor = None
                    it_count += 1
            if neighbor == None:
                filler = Filler(0, get_random_single_qubit_operation(place, ncbits, measurement_chance))
                place.assign_filler(filler)
                fillers.append(filler)
            else:
                operation = get_random_two_qubit_operation(place, neighbor_place)
                filler0 = Filler(0, operation)
                filler1 = Filler(1, operation)
                w_way = random.randint(0, 1)
                places = [place, neighbor_place]
                places[w_way].assign_filler(filler0)
                places[1-w_way].assign_filler(filler1)
                resp_vertex = Vertex(Filler_Based_Vertex_Content(filler0))
                replace_station_by(places[w_way].resp_station, Multi_Gate_Station([places[w_way], places[1-w_way]], 0, resp_vertex))
                replace_station_by(places[1-w_way].resp_station, Multi_Gate_Station([places[w_way], places[1-w_way]], 1, resp_vertex))
                fillers.append(filler0)
                fillers.append(filler1)

def fill_layer_up_with_measurements(layer, grid_width, grid_height, ncbits, fillers):
    places_to_fill = []
    for row in layer:
        for place in row:
            filler = Filler(0, get_random_single_qubit_measurement_operation(place, ncbits))
            place.assign_filler(filler)
            fillers.append(filler)

class Place:
    def __init__(self, x, y, t):
        self.x = x
        self.y = y
        self.t = t
        self.assigned_filler = None
        self.assigned_qube = None
        self.assigned_logical_qubit = None
        self.assigned_filler_based_vertex = None
        station = Place_Station(self)
        self.resp_station = station
        self.station_before = station
        self.station_after = station
    
    def is_free(self):
        return (self.assigned_filler == None and self.assigned_qube == None)
    
    def assign_filler(self, filler):
        self.assigned_filler = filler
    
    def assign_qube(self, qube):
        self.assigned_qube = qube
    
    def assign_logical_qubit(self, logical_qubit):
        self.assigned_logical_qubit = logical_qubit
    
    def to_position_string(self):
        return "p#(" + str(self.x) + "," + str(self.y) + ")"

def block_places_by_qube(tensor_of_places, spatial_position, start_time, end_time, qube):
    for time_point in range(start_time, end_time):
        tensor_of_places[time_point][qube.y + spatial_position.y_coord][qube.x + spatial_position.x_coord].assign_qube(qube)

class Station:
    def assign_logical_qubit(self, logical_qubit):
        raise Exception("AbstractMethodError!")
    
    def create_vertex(self):
        raise Exception("AbstractMethodError!")
    
    def get_input_position(self):
        raise Exception("AbstractMethodError!")
    
    def get_output_position(self):
        raise Exception("AbstractMethodError!")
    
    def is_start_or_end(self):
        raise Exception("AbstractMethodError!")
    
    def demands_realloc(self):
        raise Exception("AbstractMethodError!")
    
    def debug_print_place(self):
        raise Exception("AbstractMethodError!")

class Place_Station(Station):
    def __init__(self, place):
        self.place = place
        self.prev_station = None
        self.next_station = None
    
    def assign_logical_qubit(self, logical_qubit):
        self.place.assign_logical_qubit(logical_qubit)
    
    def get_vertex(self, logical_qubit):
        if self.place.assigned_filler_based_vertex == None:
            if self.place.assigned_filler == None:
                raise Exception("Shouldn't happen!")
            filler_based_vertex = Vertex(Filler_Based_Vertex_Content(self.place.assigned_filler))
            self.place.assigned_filler_based_vertex = filler_based_vertex
            return filler_based_vertex
        else:
            return self.place.assigned_filler_based_vertex
    
    def get_input_position(self):
        return Spatial_Position(self.place.x, self.place.y)
    
    def get_output_position(self):
        return Spatial_Position(self.place.x, self.place.y)
    
    def forbids_realloc_before(self):
        return False
    
    def forbids_realloc_after(self):
        return self.place.assigned_filler.forbids_realloc_after()
    
    def demands_realloc(self):
        return False
    
    def debug_print_place(self):
        print("(" + str(self.place.x) + "," + str(self.place.y) + "," + str(self.place.t) + ")")

class Multi_Gate_Station(Station):
    def __init__(self, places, index, resp_vertex):
        self.places = places
        self.place = places[0]
        self.index = index
        self.prev_station = None
        self.next_station = None
        self.resp_vertex = resp_vertex
    
    def assign_logical_qubit(self, logical_qubit):
        self.places[self.index].assign_logical_qubit(logical_qubit)
    
    def get_vertex(self, logical_qubit):
        if self.place.assigned_filler_based_vertex == None:
            filler_based_vertex = self.resp_vertex
            self.place.assigned_filler_based_vertex = filler_based_vertex
            return filler_based_vertex
        else:
            return self.place.assigned_filler_based_vertex
    
    def get_input_position(self):
        return Spatial_Position(self.places[self.index].x, self.places[self.index].y)
    
    def get_output_position(self):
        return Spatial_Position(self.places[self.index].x, self.places[self.index].y)
    
    def forbids_realloc_before(self):
        return False
    
    def forbids_realloc_after(self):
        return False
    
    def demands_realloc(self):
        return False
    
    def debug_print_place(self):
        print("(" + str(self.places[self.index].x) + "," + str(self.places[self.index].y) + "," + str(self.places[self.index].t) + ")")

class Qube_Station(Station):
    def __init__(self, qube, kind_id, index):
        self.qube = qube
        self.kind_id = kind_id
        self.index = index
        self.prev_station = None
        self.next_station = None
    
    def assign_logical_qubit(self, logical_qubit):
        self.qube.io_info_data[self.kind_id][self.index].logical_qubit = logical_qubit
    
    def get_vertex(self, logical_qubit):
        return self.qube.resp_vertex
    
    def get_input_position(self):
        relative_position = self.qube.io_info_data[self.kind_id][self.index].input_position
        return Spatial_Position(self.qube.x + relative_position.x_coord, self.qube.y + relative_position.y_coord)
    
    def get_output_position(self):
        relative_position = self.qube.io_info_data[self.kind_id][self.index].output_position
        return Spatial_Position(self.qube.x + relative_position.x_coord, self.qube.y + relative_position.y_coord)
    
    def forbids_realloc_before(self):
        return False
    
    def forbids_realloc_after(self):
        return False
    
    def demands_realloc(self):
        return (self.kind_id == n_ancilla)
    
    def debug_print_place(self):
        in_x = self.qube.x + self.qube.io_info_data[self.kind_id][self.index].input_position.x_coord
        in_y = self.qube.y + self.qube.io_info_data[self.kind_id][self.index].input_position.y_coord
        in_t = self.qube.start_time
        out_x = self.qube.x + self.qube.io_info_data[self.kind_id][self.index].output_position.x_coord
        out_y = self.qube.y + self.qube.io_info_data[self.kind_id][self.index].output_position.y_coord
        out_t = self.qube.end_time - 1
        print("(" + str(in_x) + "," + str(in_y) + "," + str(in_t) + ") .. (" + str(out_x) + "," + str(out_y) + "," + str(out_t) + ")")

class Start_Station(Station):
    def __init__(self):
        self.prev_station = None
        self.next_station = None
    
    def assign_logical_qubit(self, logical_qubit):
        logical_qubit.start_station = self
    
    def get_vertex(self, logical_qubit):
        return logical_qubit.get_alloc_vertex()
    
    def get_input_position(self):
        raise Exception("Shouldn't happen!")
    
    def get_output_position(self):
        raise Exception("Shouldn't happen!")
    
    def forbids_realloc_before(self):
        return True
    
    def forbids_realloc_after(self):
        return True
    
    def demands_realloc(self):
        return False
    
    def debug_print_place(self):
        print("{")

class End_Station(Station):
    def __init__(self):
        self.prev_station = None
        self.next_station = None
    
    def assign_logical_qubit(self, logical_qubit):
        logical_qubit.end_station = self
    
    def get_vertex(self, logical_qubit):
        return logical_qubit.get_free_vertex()
    
    def get_input_position(self):
        raise Exception("Shouldn't happen!")
    
    def get_output_position(self):
        raise Exception("Shouldn't happen!")
    
    def forbids_realloc_before(self):
        return True
    
    def forbids_realloc_after(self):
        return True
    
    def demands_realloc(self):
        return False
    
    def debug_print_place(self):
        print("}\n")

def do_initial_station_routing(grid_width, grid_height, grid_time, tensor_of_places, start_stations, end_stations):
    for time_point in range(0, grid_time + 1):
        if time_point > 0:
            prev_layer = tensor_of_places[time_point - 1]
        if time_point < grid_time:
            next_layer = tensor_of_places[time_point]
        for y in range(0, grid_height):
            if time_point > 0:
                prev_row = prev_layer[y]
            if time_point < grid_time:
                next_row = next_layer[y]
            for x in range(0, grid_width):
                if time_point > 0:
                    prev_station = prev_row[x].resp_station
                else:
                    prev_station = start_stations[y][x]
                if time_point < grid_time:
                    next_station = next_row[x].resp_station
                else:
                    next_station = end_stations[y][x]
                prev_station.next_station = next_station
                next_station.prev_station = prev_station

def replace_station_by(station_to_replace, station_to_replace_with):
    station_to_replace.prev_station.next_station = station_to_replace_with
    station_to_replace.next_station.prev_station = station_to_replace_with
    station_to_replace_with.prev_station = station_to_replace.prev_station
    station_to_replace_with.next_station = station_to_replace.next_station

def traverse_logical_qubit(start_station, all_logical_qubits, vertices, edges, realloc_chance):
    start_logical_qubit = Logical_Qubit()
    all_logical_qubits.append(start_logical_qubit)
    crr_logical_qubit = start_logical_qubit
    crr_station = start_station
    last_vertex = None
    saved_vertex = None
    while crr_station != None:
        if not (crr_station.forbids_realloc_before() or crr_station.prev_station.forbids_realloc_after()):
            if crr_station.demands_realloc() or (random.uniform(0.0, 1.0) < realloc_chance):
                crr_logical_qubit.end_station = End_Station()
                crr_logical_qubit.end_station.prev_station = crr_station.prev_station
                free_vertex = crr_logical_qubit.get_free_vertex()
                vertices.append(free_vertex)
                edges.append(Edge(last_vertex, free_vertex))
                saved_vertex = last_vertex
                crr_logical_qubit = Logical_Qubit()
                all_logical_qubits.append(crr_logical_qubit)
                alloc_vertex = crr_logical_qubit.get_alloc_vertex()
                vertices.append(alloc_vertex)
                last_vertex = alloc_vertex
                crr_logical_qubit.start_station = Start_Station()
                crr_logical_qubit.start_station.next_station = crr_station
        crr_vertex = crr_station.get_vertex(crr_logical_qubit)
        if crr_vertex != None:
            if saved_vertex != None:
                edges.append(Edge(saved_vertex, crr_vertex))
                saved_vertex = None
            vertices.append(crr_vertex)
            if last_vertex != None:
                edges.append(Edge(last_vertex, crr_vertex))
            last_vertex = crr_vertex
        crr_station.assign_logical_qubit(crr_logical_qubit)
        #crr_station.debug_print_place()
        crr_station = crr_station.next_station

class Edge:
    def __init__(self, vertex_from, vertex_to):
        if vertex_from == None or vertex_to == None:
            raise Exception("Shouldn't happen!")
        self.vertex_from = vertex_from
        self.vertex_to = vertex_to
        vertex_from.delta_out.append(self)
        vertex_to.delta_in.append(self)

class Vertex:
    def __init__(self, vertex_content):
        self.vertex_content = vertex_content
        self.delta_in = []
        self.delta_out = []
        self.in_count = 0
    
    def remove_from_in_counting(self, starting_vertex_set):
        starting_vertex_set.remove(self)
        for edge_to_other_vertex in self.delta_out:
            other_vertex = edge_to_other_vertex.vertex_to
            other_vertex.in_count -= 1
            if other_vertex.in_count == 0:
                starting_vertex_set.add(other_vertex)
    
    def to_gate(self):
        return self.vertex_content.to_gate()

class Graph:
    def __init__(self, vertex_list, edge_list):
        self.vertex_list = vertex_list
        self.edge_list = edge_list
    
    def get_random_directed_list(self):
        for edge in self.edge_list:
            edge.vertex_to.in_count += 1
        starting_vertex_set = set([])
        for vertex in self.vertex_list:
            if vertex.in_count == 0:
                starting_vertex_set.add(vertex)
        directed_list = []
        while len(starting_vertex_set) > 0:
            next_vertex = random.choice(list(starting_vertex_set))
            next_vertex.remove_from_in_counting(starting_vertex_set)
            directed_list.append(next_vertex)
        return directed_list

class Logical_Qubit:
    def __init__(self):
        self.lq_index = 0
        self.alloc_vertex = Vertex(Qalloc_Vertex_Content(self))
        self.free_vertex = Vertex(Qfree_Vertex_Content(self))
        self.start_station = None
        self.end_station = None
    
    def assign_index(self, lq_index):
        self.lq_index = lq_index
    
    def get_alloc_vertex(self):
        return self.alloc_vertex
    
    def get_free_vertex(self):
        return self.free_vertex

class Spatial_Position:
    def __init__(self, x_coord, y_coord):
        self.x_coord = x_coord
        self.y_coord = y_coord
    
    def to_position_string(self):
        return "p#(" + str(self.x_coord) + "," + str(self.y_coord) + ")"

class IO_Qubit_Info:
    def __init__(self, qube, kind_id, input_position, output_position, index):
        self.input_position = input_position
        self.output_position = output_position
        self.index = index
        self.logical_qubit = None
        self.resp_station = Qube_Station(qube, kind_id, index)
    
    def get_def_input_repr(self, qube, orientation):
        return "q#" + str(self.index) + ":" + orp(qube, self.input_position, orientation).to_position_string()
    
    def get_def_output_repr(self, qube, orientation):
        return "q#" + str(self.index) + ":" + orp(qube, self.output_position, orientation).to_position_string()

def check_available(tensor_of_places, x_coord, y_coord, start_time, end_time):
    for time_point in range(start_time, end_time):
        if not tensor_of_places[time_point][y_coord][x_coord].is_free():
            return False
    return True

def orp(qube, pos, orientation):
    if orientation == 0:
        return Spatial_Position(pos.x_coord, pos.y_coord)
    elif orientation == 1: #width <-> height
        return Spatial_Position(pos.y_coord, qube.width - 1 - pos.x_coord)
    elif orientation == 2:
        return Spatial_Position(qube.width - 1 - pos.x_coord, qube.height - 1 - pos.y_coord)
    elif orientation == 3: #width <-> height
        return Spatial_Position(qube.height - 1 - pos.y_coord, pos.x_coord)
    elif orientation == 4:
        return Spatial_Position(qube.width - 1 - pos.x_coord, pos.y_coord)
    elif orientation == 5: #width <-> height
        return Spatial_Position(qube.height - 1 - pos.y_coord, qube.width - 1 - pos.x_coord)
    elif orientation == 6:
        return Spatial_Position(pos.x_coord, qube.height - 1 - pos.y_coord)
    else: #width <-> height
        return Spatial_Position(pos.y_coord, pos.x_coord)

n_ancilla = 0
n_borrow = 1
n_data = 2
n_unused = 3

class Qube:
    def __init__(self, x, y, width, height, start_time, end_time, chessboard_tag, orientation):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.start_time = start_time
        self.end_time = end_time
        self.chessboard_tag = chessboard_tag
        self.name_index = 0
        self.database_index = 0
        self.orientation = orientation
        self.resp_vertex = Vertex(Qube_Based_Vertex_Content(self))
        self.io_info_data = None
    
    def occupy_places(self, tensor_of_places, start_stations, end_stations, qube_holes_coeff):
        available_positions = []
        blocked_positions = []
        position_count = 0
        for y_coord in range(0, self.height):
            for x_coord in range(0, self.width):
                if check_available(tensor_of_places, self.x + x_coord, self.y + y_coord, self.start_time, self.end_time):
                    available_positions.append(Spatial_Position(x_coord, y_coord))
                    if random.uniform(0.0, 1.0) >= qube_holes_coeff or position_count == 0:
                        position_count += 1
                else:
                    blocked_positions.append(Spatial_Position(x_coord, y_coord))
        random.shuffle(available_positions)
        used_positions = available_positions[:position_count]
        input_positions = list(map(lambda x : x, used_positions))
        output_positions = list(map(lambda x : x, used_positions))
        random.shuffle(input_positions)
        random.shuffle(output_positions)
        io_info_data = [[],[],[],[]]
        kind_count = [0,0,0,0]
        b_first = True
        for i in range(0, position_count):
            if b_first:
                r = 0
                b_first = False
            else:
                r = random.randint(0, 3)
            if r == 0 or r == 1:
                kind_id = n_data
            elif r == 2:
                kind_id = n_borrow
            elif r == 3:
                kind_id = n_ancilla
            crr_index = len(io_info_data[kind_id])
            io_info_data[kind_id].append(IO_Qubit_Info(self, kind_id, input_positions[i], output_positions[i], crr_index))
            block_places_by_qube(tensor_of_places, input_positions[i], self.start_time, self.end_time, self)
        for i in range(position_count, len(input_positions)):
            kind_id = n_unused
            crr_index = len(io_info_data[kind_id])
            io_info_data[kind_id].append(IO_Qubit_Info(self, kind_id, available_positions[i], available_positions[i], crr_index))
        self.io_info_data = io_info_data
    
    def reroute_stations(self, tensor_of_places, start_stations, end_stations):
        grid_time = len(tensor_of_places)
        if self.start_time > 0:
            prev_layer = tensor_of_places[self.start_time - 1]
        layer = tensor_of_places[self.start_time]
        for kind_id in range(0, 3):
            qubit_args = self.io_info_data[kind_id]
            for qubit_arg in qubit_args:
                next_station = qubit_arg.resp_station
                input_position = qubit_arg.input_position
                if self.start_time > 0:
                    prev_place = prev_layer[self.y + input_position.y_coord][self.x + input_position.x_coord]
                    prev_station = prev_place.station_after
                    layer[self.y + input_position.y_coord][self.x + input_position.x_coord].station_before = next_station
                else:
                    prev_station = start_stations[self.y + input_position.y_coord][self.x + input_position.x_coord]
                prev_station.next_station = next_station
                next_station.prev_station = prev_station
        if self.end_time < grid_time:
            next_layer = tensor_of_places[self.end_time]
        layer = tensor_of_places[self.end_time - 1]
        for kind_id in range(0, 3):
            qubit_args = self.io_info_data[kind_id]
            for qubit_arg in qubit_args:
                prev_station = qubit_arg.resp_station
                output_position = qubit_arg.output_position
                if self.end_time < grid_time:
                    next_place = next_layer[self.y + output_position.y_coord][self.x + output_position.x_coord]
                    next_station = next_place.station_before
                    layer[self.y + output_position.y_coord][self.x + output_position.x_coord].station_after = prev_station
                else:
                    next_station = end_stations[self.y + output_position.y_coord][self.x + output_position.x_coord]
                prev_station.next_station = next_station
                next_station.prev_station = prev_station
    
    def does_critically_overlap(self, other_qube, tolerance_coeff):
        x_tolerance = int((0.0 + self.width + other_qube.width) * (0.5 * tolerance_coeff) + 0.5)
        if self.x >= other_qube.x + other_qube.width + x_tolerance:
            return False
        if self.x + self.width < other_qube.x - x_tolerance:
            return False
        y_tolerance = int((0.0 + self.height + other_qube.height) * (0.5 * tolerance_coeff) + 0.5)
        if self.y >= other_qube.y + other_qube.height + y_tolerance:
            return False
        if self.y + self.height < other_qube.y - y_tolerance:
            return False
        time_tolerance = 0
        if self.start_time >= other_qube.end_time + time_tolerance:
            return False
        if self.end_time < other_qube.start_time - time_tolerance:
            return False
        return True
    
    def convered_places_count(self):
        return self.width * self.height * (self.end_time - self.start_time)
    
    def io_bits(self, n_io, n_kind):
        qubit_arg_list = self.io_info_data[n_kind]
        if n_io == 0:
            return ", ".join(list(map(lambda io_qubit_info : io_qubit_info.get_def_input_repr(self, self.orientation), qubit_arg_list)))
        else:
            return ", ".join(list(map(lambda io_qubit_info : io_qubit_info.get_def_output_repr(self, self.orientation), qubit_arg_list)))
    
    def to_definition_string(self):
        actual_width = self.width
        actual_height = self.height
        if self.orientation % 2 == 1:
            actual_width = self.height
            actual_height = self.width
        qube_str = "  Opague Qube " + "db" + str(self.database_index) + " (time = " + str(self.end_time - self.start_time) + ") X" + str(self.name_index)
        qube_str += " [" + str(actual_width) + ", " + str(actual_height) + "]\n    input data = (" + self.io_bits(0, n_data) + ")\n    input zero = (" + self.io_bits(0, n_ancilla)
        qube_str += ")\n    input borrowed = (" + self.io_bits(0, n_borrow) + ")\n    output data = (" + self.io_bits(1, n_data) + ")\n    output zero = (" + self.io_bits(1, n_ancilla)
        qube_str += ")\n    output borrowed = (" + self.io_bits(1, n_borrow) + ")\n  ;\n  \n"
        return qube_str
    
    def to_invocation_string(self):
        qube_str = "  Qube X" + str(self.name_index) + "("
        b_first = True
        for io_qubit_info in self.io_info_data[n_data]:
            if b_first:
                b_first = False
            else:
                qube_str += ", "
            qube_str += "q#" + str(io_qubit_info.logical_qubit.lq_index)
        qube_str += ");\n"
        return qube_str
    
    def to_solution_string(self):
        qube_str = "  Qube X" + str(self.name_index) + " (orientation = " + str(self.orientation) + ") ("
        b_first = True
        for io_qubit_info in self.io_info_data[n_data]:
            if b_first:
                b_first = False
            else:
                qube_str += ", "
            pp = io_qubit_info.input_position
            qube_str += "q#" + str(io_qubit_info.logical_qubit.lq_index) + ":p(" + str(self.x + pp.x_coord) + "," + str(self.y + pp.y_coord) + ")"
        qube_str += ");\n"
        return qube_str

def sample_nice_size(bound, reference_size):
    candidate1 = random.randint(1, 2 * reference_size - 1)
    candidate2a = random.randint(1, bound - 1)
    candidate2b = random.randint(1, bound - 1)
    if candidate2a < candidate2b:
        candidate2 = candidate2a
    else:
        candidate2 = candidate2b
    chosen_barrier = random.randint(1, 2 * reference_size - 1)
    if candidate2 < chosen_barrier and random.randint(0,2) != 0:
        return candidate2
    else:
        if candidate1 > bound or (random.randint(0,2) == 0 and random.randint(0, candidate2) < reference_size + 2):
            return candidate2
        else:
            return candidate1

def sample_nice_interval(bound, reference_size):
    if bound <= 0:
        return 0, 0
    if bound == 1:
        return 0, 1
    size = sample_nice_size(bound, reference_size)
    return random.randint(0, (bound - size + 1) - 1), size

def sample_qube(grid_width, grid_height, grid_time, grid_chessboard_tag, reference_length, reference_duration):
    x, width = sample_nice_interval(grid_width, reference_length)
    y, height = sample_nice_interval(grid_height, reference_length)
    start_time, duration = sample_nice_interval(grid_time-1, reference_duration)
    qube_chessboard_tag = get_corrected_chessboard_tag(x, y, grid_chessboard_tag)
    end_time = start_time + duration
    orientation = random.randint(0,7)
    qube = Qube(x, y, width, height, start_time, end_time, qube_chessboard_tag, orientation)
    return qube

def assign_name_to_qubes(qubes):
    qube_count = len(qubes)
    shuffled_list_for_names = range(0, qube_count)
    random.shuffle(shuffled_list_for_names)
    shuffled_list_for_dbs = range(0, qube_count)
    random.shuffle(shuffled_list_for_dbs)
    for i in range(0, qube_count):
        qubes[i].name_index = shuffled_list_for_names[i]
        qubes[i].database_index = shuffled_list_for_dbs[i]

def create_new_logical_qubit():
    return Logical_Qubit()

def shuffle_logical_qubits(logical_qubits):
    logical_qubits_count = len(logical_qubits)
    shuffled_indices = range(0, logical_qubits_count)
    random.shuffle(shuffled_indices)
    for i in range(0, logical_qubits_count):
        logical_qubits[i].assign_index(shuffled_indices[i])

class Instance:
    def __init__(self, hardware_id, all_logical_qubits, ncbits, qubes, gate_list):
        self.hardware_id = hardware_id
        self.all_logical_qubits = all_logical_qubits
        self.ncbits = ncbits
        self.qubes = qubes
        self.gate_list = gate_list
    
    def generate_messed_up_code(self):
        hardware_id = self.hardware_id
        all_logical_qubits = self.all_logical_qubits
        ncbits = self.ncbits
        qubes = self.qubes
        gate_list = self.gate_list
        below_code = "\nbegin_header\n  nqubits " + str(len(all_logical_qubits)) + "\n  ncbits " + str(ncbits) + "\n  hardware_id \"" + hardware_id + "\"\nend_header\n\n"
        below_code += "begin_qubes\n" + "".join(list(map(lambda qube : qube.to_definition_string(), qubes))) + "end_qubes\n\n"
        below_code += "begin_main\n" + "".join(list(map(lambda gate : gate.to_invocation_string(), gate_list))) + "end_main\n\n"
        return below_code
    
    def generate_solution_code(self):
        hardware_id = self.hardware_id
        all_logical_qubits = self.all_logical_qubits
        ncbits = self.ncbits
        qubes = self.qubes
        gate_list = self.gate_list
        below_code = "\nbegin_header\n  nqubits " + str(len(all_logical_qubits)) + "\n  ncbits " + str(ncbits) + "\n  hardware_id \"" + hardware_id + "\"\nend_header\n\n"
        below_code += "begin_qubes\n" + "".join(list(map(lambda qube : qube.to_definition_string(), qubes))) + "end_qubes\n\n"
        below_code += "begin_main\n" + "".join(list(map(lambda gate : gate.to_solution_string(), gate_list))) + "end_main\n\n"
        return below_code

def generate_instance(configs):
    grid_width = configs.grid_width
    grid_height = configs.grid_height
    grid_time = configs.grid_time
    target_qube_covering_factor = configs.target_qube_covering_factor
    realloc_chance = configs.realloc_chance
    failure_max = configs.failure_max
    tolerance_coeff = configs.tolerance_coeff
    ncbits = configs.ncbits
    chessboard_tag = configs.chessboard_tag
    reference_qube_length = configs.reference_qube_length
    reference_qube_duration = configs.reference_qube_duration
    qube_holes_coeff = configs.qube_holes_coeff
    measurement_chance = configs.measurement_chance
    b_hot = (configs.hot_cold_id == 1)
    if not b_hot:
        target_qube_covering_count = int(target_qube_covering_factor * grid_width * grid_height * grid_time + 0.5)
        covered_count = 0
        qubes = []
        failure_count = 0
        while covered_count < target_qube_covering_count and failure_count < failure_max:
            qube = sample_qube(grid_width, grid_height, grid_time, configs.chessboard_tag, reference_qube_length, reference_qube_duration)
            b_overlap_found = False
            for other_qube in qubes:
                if qube.does_critically_overlap(other_qube, tolerance_coeff):
                    b_overlap_found = True
            if not b_overlap_found:
                covered_count = covered_count + qube.convered_places_count()
                if covered_count < target_qube_covering_count:
                    qubes.append(qube)
                else:
                    if random.randint(0, 1):
                        qubes.append(qube)
                failure_count = 0
            else:
                failure_count += 1
        assign_name_to_qubes(qubes)
    else:
        qubes = []
    tensor_of_places = list(map(lambda t : list(map(lambda y : list(map(lambda x : Place(x,y,t), range(grid_width))), range(grid_height))), range(grid_time)))
    start_stations = list(map(lambda y : list(map(lambda x : Start_Station(), range(grid_width))), range(grid_height)))
    end_stations = list(map(lambda y : list(map(lambda x : End_Station(), range(grid_width))), range(grid_height)))
    do_initial_station_routing(grid_width, grid_height, grid_time, tensor_of_places, start_stations, end_stations)
    for qube in qubes:
        qube.occupy_places(tensor_of_places, start_stations, end_stations, qube_holes_coeff)
        qube.reroute_stations(tensor_of_places, start_stations, end_stations)
    all_fillers = []
    for time_point in range(0, grid_time-1):
        layer = tensor_of_places[time_point]
        fill_layer_up_with_elem(layer, grid_width, grid_height, chessboard_tag, ncbits, measurement_chance, all_fillers)
    fill_layer_up_with_measurements(tensor_of_places[grid_time - 1], grid_width, grid_height, ncbits, all_fillers)
    all_logical_qubits = []
    vertices = []
    edges = []
    for y_coord in range(0, grid_height):
        start_stations_row = start_stations[y_coord]
        for x_coord in range(0, grid_width):
            start_station = start_stations_row[x_coord]
            traverse_logical_qubit(start_station, all_logical_qubits, vertices, edges, realloc_chance)
    shuffle_logical_qubits(all_logical_qubits)
    dependency_graph = Graph(vertices, edges)
    dependency_vertex_list = dependency_graph.get_random_directed_list()
    gate_list = list(map(lambda vertex : vertex.to_gate(), dependency_vertex_list))
    hardware_id = "grid" + str(chessboard_tag) + "n" + str(grid_width) + "n" + str(grid_height)
    return Instance(hardware_id, all_logical_qubits, ncbits, qubes, gate_list)

class Configurations:
    def __init__(self):
        self.grid_width = 8
        self.grid_height = 8
        self.grid_time = 12
        self.target_qube_covering_factor = 0.8
        self.realloc_chance = 0.1
        self.failure_max = 20
        self.tolerance_coeff = 0.0
        self.ncbits = 8
        self.chessboard_tag = 0
        self.reference_qube_length = 4
        self.reference_qube_duration = 6
        self.qube_holes_coeff = 0.0
        self.hot_cold_id = 0 #0 = cold ; 1 = hot
        self.measurement_chance = 0.25

def read_file(file_name):
    file_handler = open(file_name,"r")
    text = file_handler.read()
    file_handler.close()
    return text

def write_file(file_name, text):
    file_handler = open(file_name,"w")
    file_handler.write(text)
    file_handler.close()

def read_off_configs(input_string):
    configs = Configurations()
    splitted_input_string = input_string.split("\n")
    for line in splitted_input_string:
        parts = line.split(":")
        if len(parts) == 2:
            prop = parts[0]
            val = parts[1]
            if prop == "grid_width":
                configs.grid_width = int(val)
            if prop == "grid_height":
                configs.grid_height = int(val)
            if prop == "grid_time":
                configs.grid_time = int(val)
            if prop == "target_qube_covering_factor":
                configs.target_qube_covering_factor = float(val)
            if prop == "ralloc_chance":
                configs.realloc_chance = float(val)
            if prop == "failure_max":
                configs.failure_max = float(val)
            if prop == "tolerance_coeff":
                configs.tolerance_coeff = float(val)
            if prop == "ncbits":
                configs.ncbits = int(val)
            if prop == "chessboard_tag":
                configs.chessboard_tag = int(val)
            if prop == "reference_qube_length":
                configs.reference_qube_length = int(val)
            if prop == "reference_qube_duration":
                configs.reference_qube_duration = int(val)
            if prop == "qube_holes_coeff":
                configs.qube_holes_coeff = float(val)
            if prop == "hot_cold_id":
                configs.hot_cold_id = int(val)
            if prop == "measurement_chance":
                configs.measurement_chance = float(val)
    return configs

def main():
    input_file_name = "input.txt"
    output_file_name = "output.txt"
    solution_file_name = "solution.txt"
    input_string = read_file(input_file_name)
    configs = read_off_configs(input_string)
    instance = generate_instance(configs)
    output_string = instance.generate_messed_up_code()
    write_file(output_file_name, output_string)
    solution_string = instance.generate_solution_code()
    write_file(solution_file_name, solution_string)

main()


