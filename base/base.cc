// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 24, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// base/base.cc
//
// GNU C++17
//
#include "base.hh"


#include <iostream>


static
int test()
{
     if (  sizeof(XY_t) != sizeof(uint32_t) || alignof(XY_t) != alignof(uint32_t)
           ||
           sizeof(CReg_with_Idx_t) != sizeof(uint32_t) || alignof(CReg_with_Idx_t) != alignof(uint32_t)  )
     {
          std::cerr<<"\n"
               "base.cc: test(): The platform you're running the compiler on doesn't conform to the requirements.\n"
               "Be part of the revolution: Adapt the source code to your platform!\n";
          throw "base.cc: test(): Sad. :(";
     }
     return 0;
}

static int dummy = test();

// EOF
