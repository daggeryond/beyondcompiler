// backends/bristlecone.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created May 25, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This file contains the descriptions of the Google Bristlecone backend.
//
// GNU C++17
//
#ifndef __BACKENDS_BRISTLECONE_HH__
#define __BACKENDS_BRISTLECONE_HH__

namespace Backends {

     struct Bristlecone
     {
          static constexpr Backend_ID_t id       {"Google_Bristlecone"};
          static constexpr int          n_qubits = 72;
          static constexpr Coupling_tag coupling = Coupling_tag::grid_O;

          typedef Base::XY_t XY_t;


          // Decide if xy-coordinate is a qubit address:
                                             // corners are: (5.5,-.5), (11,5), (5.5,10.5), (0,5)
          static inline                      //         (5.5,-.5)--(11,5)        (11,5)--(5.5,10.5)         (5.5,10.5)--(0,5)        (5.5,0)--(0,5)
          bool inside(XY_t xy)               {  return  xy.x - xy.y <= 6    &&   xy.x + xy.y <= 16    &&    xy.y - xy.x <= 5    &&   xy.x + xy.y >= 5;  }



          // Iterating over all qubits
          // -------------------------

          struct Iter_t                /* Iterator type */   { XY_t xy; XY_t operator*() const {return xy;} };

          constexpr Iter_t begin() const                               { return Iter_t{XY_t{5, 0}}; }
          constexpr Iter_t end()   const                               { return Iter_t{XY_t{6,11}}; }

     };

     inline
     Bristlecone::Iter_t & operator ++ (Bristlecone::Iter_t&i)                           { ++i.xy.x; if (!Bristlecone::inside(i.xy)) {++i.xy.y; i.xy.x=Base::X_t{(short)std::abs(i.xy.y-5)};} return i; }
     inline
     bool                  operator != (Bristlecone::Iter_t&i,Bristlecone::Iter_t&j)     { return !( i.xy.x==j.xy.x && i.xy.y==j.xy.y ); }

} //^ namespace Backends

#endif // EOF