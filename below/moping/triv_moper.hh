// below/moping/triv_moper.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 28, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
// This file contains the declaration of DOT's trivial mapper.
//
// GNU C++17
//
#ifndef __BELOW_TRIV_MOPING_HH__
#define __BELOW_TRIV_MOPING_HH__

#include "moping.hh"


namespace Below {
     namespace Moping {

          Result_t dots_triv_moper(Below_Prg *, const Options_t &);

     }
}

