// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created May 25, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// base/base.hh
//
// This file contains basic project-global types and aux functions.
// See also: base.cc
//
// GNU C++17
//
#ifndef __BELOW_BASE_HH__
#define __BELOW_BASE_HH__

#include <cstddef>
#include <cstdint>

#include <stdexcept>

namespace Base {

     // Helpers for KILL THE BUG stuff
     template<typename ENUM>
     inline
     ENUM & operator += (ENUM & x, ENUM d)                     { return x=ENUM(x+d);   }

     template<typename ENUM>
     inline
     ENUM & operator -= (ENUM & x, ENUM d)                     { return x=ENUM(x-d);   }

     template<typename ENUM>
     inline
     ENUM & operator ++ (ENUM & x)                             { return x+=ENUM{1};    }

     //
     // Data Structures for Qubits, Addresses, etc
     // ------------------------------------------
     //

     //
     // Fridge Data
     //
     // Fridge data, aka classical registers.  Addresses are 32 bit

     enum CReg_t : uint16_t { CReg_zero=0, Void_CReg=UINT16_MAX-3 };

     template CReg_t & operator+=(CReg_t & x, CReg_t d);
     template CReg_t & operator++(CReg_t & x);

     enum CReg_Idx_t : uint16_t { CReg_Idx_zero=0, Void_CReg_Idx=UINT16_MAX-3 };

     template CReg_Idx_t & operator+=(CReg_Idx_t & x, CReg_Idx_t d);
     template CReg_Idx_t & operator++(CReg_Idx_t & x);


     struct CReg_with_Idx_t
     {
          CReg_t      creg                          = Void_CReg;
          CReg_Idx_t  idx                           = Void_CReg_Idx;
     };
     bool operator==(CReg_with_Idx_t cwi, CReg_with_Idx_t cwii)            { return *(uint32_t*)(&cwi) == *(uint32_t*)(&cwii); }
     bool operator!=(CReg_with_Idx_t cwi, CReg_with_Idx_t cwii)            { return *(uint32_t*)(&cwi) != *(uint32_t*)(&cwii); }
     bool operator< (CReg_with_Idx_t cwi, CReg_with_Idx_t cwii)            { return *(uint32_t*)(&cwi) <  *(uint32_t*)(&cwii); }

     constexpr
     CReg_with_Idx_t Void_CReg_with_Idx();


     // Logical Qubits

     enum LQubit_t : int16_t { LQubit_zero=0, Void_LQubit=INT16_MIN }; // negative values greater than `Void_LQubit` can hold special information.

     template LQubit_t & operator+=(LQubit_t & x, LQubit_t d);
     template LQubit_t & operator++(LQubit_t & x);

     //
     // Orientations of Qubes
     //

     enum Orientation : int8_t { Void_Orientation=-1, Orientation_begin=0,Orientation_1=1,Orientation_2=2,Orientation_3=3,Orientation_4=4,Orientation_5=5,Orientation_6=6,Orientation_7=7, Orientation_END=8 };
     // Iteration over orientations:
#    define ALL_ORIENTATIONS(var)     Base::Orientation var=Base::Orientation_begin; var!=Base::Orientation_END; ++var
     // Use like this:
     //                       for ( ALL_ORIENTATIONS(o) ) {   ... use(o); ...   }
     // E.g.:
     //                       for ( ALL_ORIENTATIONS(o) ) {
     //                            Base::XY_t q = p.global(zero,o);
     //                            std::cout<<'('<<q.x<<','<<q.y<<")\t";
     //                       }

     //
     // Addresses
     //

     enum X_t : int16_t {};
     enum Y_t : int16_t {};

     template X_t & operator+=(X_t & x, X_t d);
     template Y_t & operator+=(Y_t & y, Y_t d);
     template X_t & operator++(X_t & x);
     template Y_t & operator++(Y_t & y);

     struct XY_t
     {
          X_t x;
          Y_t y;

          inline constexpr
          XY_t()                                          : x{-32767},y{-32767} {}

          inline constexpr
          XY_t(int _x, int _y);
          bool operator <= (XY_t uv)   const              { return 0 <= uv.x && uv.x < x && 0 <= uv.y && uv.y < y; }

          inline
          XY_t global(XY_t zero, Orientation);

          // Get index for array access:
          unsigned lin(XY_t world)                 const    { return world.x*(int)y + x; }
          unsigned lin(XY_t world, XY_t universe)  const    { world.x+=universe.x; world.y+=universe.y; return lin(world); }



          struct Hash {
               size_t operator()(XY_t xy) const           { return (size_t)xy.x + (size_t)xy.y; }
          };
     };

     constexpr
     XY_t Void_Addr{};


     XY_t operator+(XY_t xy, XY_t uv)                     { XY_t r; r.x=X_t{(short)(xy.x+uv.x)}; r.y=Y_t{(short)(xy.y+uv.y)}; return r; }

     bool operator==(XY_t xy, XY_t uv)                    { return *(uint32_t*)(&xy)==*(uint32_t*)(&uv); }
     bool operator!=(XY_t xy, XY_t uv)                    { return *(uint32_t*)(&xy)!=*(uint32_t*)(&uv); }


     typedef XY_t Addr_t;


     // Data Structures for Qubit Couplings
     // -----------------------------------

     enum class Coupling_tag {grid_O, chess_XO, chess_OX, grid_X};

     template<Coupling_tag C>
     inline
     bool q2_is_legal(XY_t xy, XY_t uv);               // Check if xy,uv can be operands to a 2-qubit operation in connectivity C.
                                                       // Throws a std::runtime_error if xy==uv

     class Runtime_Boolean_Expression {
         CReg_t c;
         int val;
     };

} //^ namespace Base

////////////////////////////////////////////////////////////////////////////////////////////////////
//
// IMPLEMENTATIONS
//
////////////////////////////////////////////////////////////////////////////////////////////////////

inline constexpr
Base::XY_t::
XY_t(const int _x, const int _y)
     : x{X_t{(int16_t)_x}},
       y{Y_t{(int16_t)_y}}
{
#    ifndef BEYOND__NODEBUG
     if (_x>16000 || _y>16000) throw std::out_of_range("Base::XY_t-constructor: This version is limited to at most 16000x16000 qubits.");
#    endif
}


inline
Base::XY_t
Base::XY_t::
global(XY_t zero, const Orientation ori)
{
     switch (ori) {
     case Orientation_begin:
          zero.x+=x, zero.y+=y;
          return zero;
     case Orientation_1:
          zero.x+=x, zero.y-=y;
          return zero;
     case Orientation_2:
          zero.x-=x, zero.y+=y;
          return zero;
     case Orientation_3:
          zero.x-=x, zero.y-=y;
          return zero;

     case Orientation_4:
          zero.x+=X_t{y}, zero.y+=Y_t{x};
          return zero;
     case Orientation_5:
          zero.x+=X_t{y}, zero.y-=Y_t{x};
          return zero;
     case Orientation_6:
          zero.x-=X_t{y}, zero.y+=Y_t{x};
          return zero;
     case Orientation_7:
          zero.x-=X_t{y}, zero.y-=Y_t{x};
          return zero;
     default:
          throw std::runtime_error("Base::XY_t::global(): Invalid orientation.");
     }
} //^ XY_t:: global()

template<Base::Coupling_tag C>
inline
bool
Base::
q2_is_legal(const XY_t xy, const XY_t uv)
{
     using std::abs, std::min;
     if (abs(xy.x-uv.x)>1 || abs(xy.y-uv.y)>1)  return false;
#    ifdef BEYOND__NODEBUG
     if ( xy==uv ) throw std::runtime_error("Below::Connectivity::is_legal(): It seems like there's an operation on two operands, but the two operands have the same address.");
#    endif
     if ( xy.x==uv.x || xy.y==uv.y ) return true;
     switch(C) {  // g++ will optimize this away (I hope)
     case Coupling_tag::grid_O:
          return false;
     case Coupling_tag::chess_XO:
          return ( min(xy.x,uv.x) + min(xy.y,uv.y) )%2  ==0;
     case Coupling_tag::chess_OX:
          return ( min(xy.x,uv.x) + min(xy.y,uv.y) )%2  ==1;
     case Coupling_tag::grid_X:
          return true;
     }
} //^ q2_is_legal()


#endif //EOF
