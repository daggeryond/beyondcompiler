
// don't forget: the return `values' from Beyond have to be dealt with (no returned stuff in Unicorn)



// ****************************************************************************************************
// *                                                                                                  *
// *  There must be a way to say: This is a Qube, Ctrl-Tree cannot brake it up.                       *
// *                                                                                                  *
// ****************************************************************************************************

// ****************************************************************************************************
// *                                                                                                  *
// *  Procedures can have qualloc/qufree                                                              *
// *                                                                                                  *
// ****************************************************************************************************

// Idea:
// Add special command(s) for ``measuring into'' a data structure (will be returned / called back to Python)
// Syntax something like: measure_array( name_of_python_callback_or_something, fixed3_2[16], data[0:12] data[24:36] zeros[0:12] data[36:48] data[50] zeros[12:24] )
// Only in procedures (I guess)


// Identifiers
// -----------
// There are 3 types of identifiers:
// 1. Complex IDs for unitaries and procedures
// 2. IDs of fridge variables
// 3. IDs of angel variables
//
// Complex IDs start with a letter or underscore and can contains stuff such as <> and . (dot).
// Fridge variable names start with a capital F
// Angel variable names start with a capital A


// Fridge
// ------
//
// In Unicorn, all fridge variables are BY REFERENCE.
// If you don't want to change them, don't change them!
// Instead, declare a local fridge variable in the unitary's body.
//
// Fridge variables can have arbitrary primitive types...
// ... but for now, let's stick to allowing only
//        FIXEDn_m   -- signed fixed point with n-1 bits before, m bits after decimal point
//                      same as (n+m)-bit signed int divided by 2^m
//        UFIXEDn_m  -- unsigned fixed point with n bits before, m bits after decimal point
//                      same as (n+m)-bit unsigned int divided by 2^m.
//
// A signed n-bit int shall be represented as FIXEDn_0
// An unsigned n-bit int shall be represented as UFIXEDn_0
// A bit shall be represented as UFIXED1_0
//
//   ***********************************************
//   *                                             *
//   *  What's the good way to do arithmetic ???   *
//   *                                             *
//   ***********************************************
//
//
// Angels
// ------
// * Angels come in arrays of IEEE 754 floats
// * Angels must not influence control flow
// * Arithmetic is allowed between individual angels, e.g., A_x[0] + A_x[1]
// * Arithmetic is allowed between angel-arrays of the same size (elementwise)
// * Maybe later add things like dot-product, or maybe not.
//
// Fridge-dependent control-flow cannot include Angels!
// There's no arithmetic between Angels (double) and fridge data (fixed).  This is because the resulting type would be `non-final fridge double`, and there is no such type currently.
//

UNITARY My_unitary( data[61]; fridge_fixed1_8 F_x, fride_ufixed0_8 F_a; A_a[8], A_b[4];  zeros[8], borrows[4] ) {
    // Declarations of local variables: ONLY on the top of the unitary/procedure
    // Declarations of local variables: ONLY fridges and angels
    fridge_ufixed4_5 F_local_number;
    angel[4] A_loc;

}




opaque_unitary          :  "OPAQUE" unitary_head ';'
                        ;

unitary                 :  unitary_head '{' unitary_body '}'
                        ;

promised_unitary        :  "PROMISED" unitary_head '{' unitary_body '}'
                        ;

promised_unitary_dagger :  "PROMISED" "DAGGER" unitary_head '{' unitary_body '}'
                        ;

promised_ctrl_unitary   :  "PROMISED" "CTRL" unitary_head '{' unitary_body '}'
                        ;

unitary_head            :  "UNITARY" T_cplx_ID '('
                                                  "data"    '[' T_nnegint[dsz] ']' ';'
                                                  fridge_params                    ';'
                                                  angel_params                     ';'
                                                  "zeros"   '[' T_nnegint[zsz] ']' ','  //  give length 0
                                                  "borrows" '[' T_nnegint[bsz] ']'     //  for `none'
                                               ')'
                        ;

fridge_params           :  %empty
                        |  fridge_decl fridge_params
                        ;

fridge_decl             :  fridge_type T_fridge_ID
                        ;

fridge_type             :  T_fixed
                        |  T_ufixed
                        ;

angel_params            :  %empty
                        |  angel_decl angel_params
                        ;

angel_decl              :  T_


// **********************************************************************
// *                                                                    *
// *   Invoking a Unitary                                               *
// *                                                                    *
// **********************************************************************
//
// Example:
//
// APPLY myunitary( data[0:12] data[24:36] zeros[0:12] data[36:48] borrows[1] zeros[12:24]; F_x+F_y-3.2, F_23<F_24; A_s[0:2] + A_t[0:2]; A_s[0:2] * A_t[0:2] );
// Unitaries can have complex IDs, e.g., My.Unitary23.3298<923<2<.<oi<28j.f<woi_._nef<ow.ienf>987>
// In comparison, here is a comparison between two fridge vars: F_dlfk<F_dnfl

invoke_unitary         :  "APPLY" T_cplx_ID '(' data_arg ';' fridge_args ';' angel_args ')' ';'
                       ;

data_arg               :  qureg_access
                       |  data_arg qureg_access
                       ;

qureg_access           :  data_access
                       |  zero_access
                       |  borrows_access
                       ;

data_access            :  "data" '[' range ']'
                       ;
zero_access            :  "zero" '[' range ']'
                       ;
borrows_access         :  "borrows" '[' range ']'
                       ;

range                  :  T_nnegint
                       |  T_nnegint ':' T_nnegint
                       ;


// Header should give backend and includes (no includes in the middle)
#include "some_stuff.unicorn"
#include "library_stuff.unicorn"
