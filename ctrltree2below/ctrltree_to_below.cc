// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 28, 2018 by TVC
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// ctrltree2below/ctrltree_to_below.cc
//
// This file contains the compilation from the ctrl-tree-format to the below-format.
//
// GNU C++17
//

namespace Ctrltreetobelow {
    class Tree_Node_In_Composed_Unitary_Tree_Node_Representer {
        bool paired_up = False;
        Tree_Node_In_Composed_Unitary_Tree_Node_Representer* partner = nullptr;
        int partner_index = -1;
    public:
        Unitary_Tree_Node *represented_tree_node;
        int index;
        
        inline
        Tree_Node_In_Composed_Unitary_Tree_Node_Representer(Unitary_Tree_Node *represented_tree_node_, int index_)
        represented_tree_node{represented_tree_node_} index{index_} {}
        
        void pair_up_with(Tree_Node_In_Composed_Unitary_Tree_Node_Representer* other);
        
        bool is_chosen_for_skipping_ctrl();
        
        Unitary_Tree_Node* get_represented_tree_node();
    }
    
    void Tree_Node_In_Composed_Unitary_Tree_Node_Representer::pair_up_with(Tree_Node_In_Composed_Unitary_Tree_Node_Representer* other) {
        paired_up = True;
        partner = other;
        partner_index = other->index;
    }
    
    bool Tree_Node_In_Composed_Unitary_Tree_Node_Representer::is_chosen_for_skipping_ctrl() {
        return paired_up;
    }
    
    Unitary_Tree_Node* Tree_Node_In_Composed_Unitary_Tree_Node_Representer::get_represented_tree_node(){
        return represented_tree_node;
    }
    
    void find_cancelling_off_subset(std::vector<Tree_Node_In_Composed_Unitary_Tree_Node_Representer*>& representers) {
        //TODO: Implement
    }
    
    Locally_Flattened_Tree_Node*
    locally_flatten_ctrl_composed_tree_node(LQubit_t ctrling_qubit, std::vector<Unitary_Tree_Node*>& ctrled_tree_nodes) {
        std::vector<Tree_Node_In_Composed_Unitary_Tree_Node_Representer*> representers;
        for(const Unitary_Tree_Node& tree_node : ctrled_tree_nodes) {
            Tree_Node_In_Composed_Unitary_Tree_Node_Representer* representer = new Tree_Node_In_Composed_Unitary_Tree_Node_Representer(...);
            representers.push_back(representer);
        }
        find_cancelling_of_subset(representers);
        std::vector<Tree_Node*> *locally_flattened_tree_nodes = new std::vector<Tree_Node*>();
        for(const Tree_Node_In_Composed_Unitary_Tree_Node_Representer* representer : representers) {
            Tree_Node *crr_tree_node = representer.get_represented_tree_node();
            if(representer.is_chosen_for_skipping_ctrl()) {
                locally_flattened_tree_nodes.push_back(representer.get_represented_tree_node());
            } else {
                std::vector<Tree_Node*> ctrled_crr_tree_node = locally_flatten_ctrl_tree_node(ctrl_qubit, crr_tree_node);
                for(const Tree_Node* tree_node : ctrled_crr_tree_node) {
                    locally_flattened_tree_nodes.push_back(tree_node);
                }
            }
        }
        return locally_flatten_composed_unitary_tree_node(locally_flattened_tree_nodes);
    }
    
    Locally_Flattened_Tree_Node*
    locally_flatten_tree_node(Tree_Node *tree_node) {
        return (*tree_node).locally_flatten(this);
    }
    
    Locally_Flattened_Tree_Node*
    locally_flatten_dagger_tree_node(Unitary_Tree_Node *daggered_tree_node) {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node*
    locally_flatten_ctrl_tree_node(LQubit_t ctrling_qubit, Unitary_Tree_Node *ctrled_tree_node) {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node*
    locally_flatten_composed_nonunitary_tree_node(std::vector<Tree_Node*>& tree_nodes) {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node*
    locally_flatten_composed_unitary_tree_node(std::vector<Unitary_Tree_Node*>& tree_nodes) {
        std::vector<Tree_Node*> *locally_flattened_tree_nodes = new std::vector<Tree_Node*>();
        for(const Tree_Node* tree_node : tree_nodes) {
            std::vector<Tree_Node*> crr_locally_flattened_tree_nodes = locally_flatten_tree_node(tree_node);
                for(const Tree_Node* inner_tree_node : crr_locally_flattened_tree_nodes) {
                    locally_flattened_tree_nodes.push_back(inner_tree_node);
                }
            }
        }
        return locally_flattened_tree_nodes;
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Composed_Nonunitary_Tree_Node::locally_flatten() {
        return locally_flatten_composed_nonunitary_tree_node(children);
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Composed_Nonunitary_Tree_Node::locally_flatten_ctrl(LQubit_t ctrling_qubit) {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Elementary_Nonunitary_Tree_Node::locally_flatten() {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Elementary_Nonunitary_Tree_Node::locally_flatten_ctrl(LQubit_t ctrling_qubit) {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Ctrl_Tree_Node::locally_flatten() {
        return locally_flatten_ctrl_tree_node(ctrling_qubit, ctrled_tree_node)
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Ctrl_Tree_Node::locally_flatten_ctrl(LQubit_t ctrling_qubit) {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Dagger_Tree_Node::locally_flatten() {
        return service.locally_flatten_dagger_tree_node(daggered_tree_node)
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Dagger_Tree_Node::locally_flatten_ctrl(LQubit_t ctrling_qubit) {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Composed_Unitary_Tree_Node::locally_flatten() {
        return service.locally_flatten_composed_unitary_tree_node(children);
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Composed_Unitary_Tree_Node::locally_flatten_ctrl(LQubit_t ctrling_qubit) {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Elementary_Unitary_Tree_Node::locally_flatten() {
        return nullptr; //TODO: Implement correctly
    }
    
    Locally_Flattened_Tree_Node* Ctrltree::Elementary_Unitary_Tree_Node::locally_flatten_ctrl(LQubit_t ctrling_qubit) {
        return nullptr; //TODO: Implement correctly
    }
    
}//^ namespace Ctrltreetobelow

//EOF

