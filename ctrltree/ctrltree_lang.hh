// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 20, 2018 by TVC
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// ctrltree/ctrltree_lang.hh
//
// This file contains the definitions of the Ctrltree language.
//
// GNU C++17
//
#ifndef __CTRLTREE_CTRLTREE_LANG_HH__
#define __CTRLTREE_CTRLTREE_LANG_HH__

#include "../base/base.hh"
#include "../traversary/traversary.hh"

namespace Ctrltree {
    
    class Tree_Node;

    using Locally_Flattened_Tree_Node = std::vector<*Tree_Node>;
    
    class Convert_to_Below_Service {
    
    public:
        //TODO: Put needed methods here!
    }
    
    class Elementary_Operation {
        Traversary::Highlevel_Elementary_Operation *unitary;
        LQubit_t qubit;
    public:
        inline
        Elementary_Operation(Traversary::Highlevel_Elementary_Operation *unitary_, LQubit_T qubit_) unitary{unitary_} qubit{qubit_} {}
        
        ~Elementary_Operation();
    }
    
    class Tree_Node {
        
    public:
        virtual Locally_Flattened_Tree_Node* locally_flatten() = 0;
    }
    
    class Nonunitary_Tree_Node : public Tree_Node {
        
    public:
        //methods implement in ctrltree_to_below
        virtual Locally_Flattened_Tree_Node* locally_flatten() = 0;
        virtual Locally_Flattened_Tree_Node* locally_flatten_ctrl(LQubit_t ctrling_qubit) = 0;
    }
    
    class Composed_Nonunitary_Tree_Node : public Nonunitary_Tree_Node {
        std::vector<Tree_Node*> children;
    
    public:
        inline
        Composed_Nonunitary_Tree_Node(std::vector<Tree_Node*> children_) children{children_} {}
        
        ~Composed_Nonunitary_Tree_Node();
        
        //methods implement in ctrltree_to_below
        Locally_Flattened_Tree_Node* locally_flatten();
        Locally_Flattened_Tree_Node* locally_flatten_ctrl(LQubit_t ctrling_qubit);
    }
    
    class Elementary_Nonunitary_Tree_Node : public Nonunitary_Tree_Node {
        Elementary_Operation contained_operation;
    public:
        inline
        Elementary_Nonunitary_Tree_Node(Elementary_Operation contained_operation_) contained_operation{contained_operation_} {}
        
        //methods implement in ctrltree_to_below
        Locally_Flattened_Tree_Node* locally_flatten();
        Locally_Flattened_Tree_Node* locally_flatten_ctrl(LQubit_t ctrling_qubit);
    }
    
    class Unitary_Tree_Node : public Tree_Node {
        
    public:
        //methods implement in ctrltree_to_below
        virtual Locally_Flattened_Tree_Node* locally_flatten() = 0;
        virtual Locally_Flattened_Tree_Node* locally_flatten_ctrl(LQubit_t ctrling_qubit) = 0;
    }
    
    class Ctrl_Tree_Node : public Unitary_Tree_Node {
        LQubit_t ctrling_qubit;
        Unitary_Tree_Node* ctrled_tree_node;
    
    public:
        inline
        Ctrl_Tree_Node(LQubit_t ctrling_qubit_, Unitary_Tree_Node* ctrled_tree_node_) ctrling_qubit{ctrling_qubit_} ctrled_tree_node{ctrled_tree_node_} {}
        
        ~Ctrl_Tree_Node();
        
        //methods implement in ctrltree_to_below
        Locally_Flattened_Tree_Node* locally_flatten();
        Locally_Flattened_Tree_Node* locally_flatten_ctrl(LQubit_t ctrling_qubit);
    }
    
    class Dagger_Tree_Node : public Unitary_Tree_Node {
        Unitary_Tree_Node* daggered_tree_node;
    
    public:
        inline
        Dagger_Tree_Node(Unitary_Tree_Node* daggered_tree_node_) daggered_tree_node{daggered_tree_node_} {}
        
        ~Dagger_Tree_Node(); 
        
        //methods implement in ctrltree_to_below
        Locally_Flattened_Tree_Node* locally_flatten();
        Locally_Flattened_Tree_Node* locally_flatten_ctrl(LQubit_t ctrling_qubit);
    }
    
    class Composed_Unitary_Tree_Node : public Unitary_Tree_Node {
        std::vector<Unitary_Tree_Node*> children;
    
    public:
        inline
        Composed_Unitary_Tree_Node(std::vector<Unitary_Tree_Node*> children_) children{children_} {}
        
        ~Composed_Unitary_Tree_Node();
        
        //methods implement in ctrltree_to_below
        Locally_Flattened_Tree_Node* locally_flatten();
        Locally_Flattened_Tree_Node* locally_flatten_ctrl(LQubit_t ctrling_qubit);
    }
    
    class Elementary_Unitary_Tree_Node : public Unitary_Tree_Node {
        Elementary_Operation contained_operation;
    public:
        inline
        Elementary_Unitary_Tree_Node(Elementary_Operation contained_operation_) contained_operation{contained_operation_} {}
        
        //methods implement in ctrltree_to_below
        Locally_Flattened_Tree_Node* locally_flatten();
        Locally_Flattened_Tree_Node* locally_flatten_ctrl(LQubit_t ctrling_qubit);
    }
    
}//^ namespace Ctrltree


#endif
//EOF
