// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created May 25, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// below/below_lang.hh
//
// This file contains the definitions of the Below language.
//
// GNU C++17
//
#ifndef __BELOW_BELOW_LANG_HH__
#define __BELOW_BELOW_LANG_HH__

// ****************************************************************************************************
// *                                                                                                  *
// *  Todo Issues                                                                                     *
// *                                                                                                  *
// ****************************************************************************************************

#warning "Todo:  * Add  Angel  stuff !!!"

#warning "Todo:  * Fill `Below_Prg`"  [DOT is working on this]
#warning "Todo:  * Check stuff for expressions on fridge register"
#warning "Todo:  * Class `compile_error` is obsolete -- I think"
#warning "Todo:  * Parser: Qube calls must identify version of qube!"
#error

#include <memory>
#include <string>
#include <sstream>


#include "../base/base.hh"
#include "../backends/backends.hh"

namespace Below {

     class Qube;
     class Operation;
     typedef std::vector< Operation * >    Operation_Sequence_t;


     class Below_Prg
     {
          friend class Below_Prg_Builder;



          Backends::Backend_ID_t *           p_backend;             // p_backend->id is a unique string identifying the backend

          std::multi<std::string,Qube*>     the_qubes;              // key is Unique Qube ID (to look up a qube name, iterate over everything)
                                                                    // All qubes with same functionality and same #lqubits have same name...
                                                                    // ...but the unique qube id is unique.
                                                                    // Before mapping:  unique_qube_id == name+"["+var+"]".
                                                                    // After mapping (= file for hot phase): unique_qube_id == concise
          Operation_Sequence_t              the_main;

          unsigned                          n_lqubits;
          std::vector<unsigned>             creg_sz;

     public:
          Backends::Backend_ID_t                    get_backend() const                           { return *p_backend; }
          const std::multi<std::string,Qube*> &     qubes()       const                           { return the_qubes; }
          const Operation_Sequence_t &              main()        const                           { return the_main; }
          unsigned                                  n_logqubits() const                           { return n_lqubits; }
          unsigned                                  n_cregs()     const                           { return creg_sz.size(); }
          const std::vector<unsigned> &             creg_size()   const                           { return creg_sz; }
          std::string to_json();
          std::string to_below_code();
     };

     // For parsing:
     struct Builder // base class for Builder classes
     {
          class Context  // Derive from this class and implement checking of operations
          {
               virtual
               bool check_if_legal(const All_Parsers::YYLTYPE &, const Operation *) const =0;
          };

     };

     class Below_Prg_Builder: public Builder
     {
          Below_Prg *                p_the_below_prg;


          using Builder::Context;
          std::stack<Context*>       context_stack;

     public:
          void push_context          (const All_Parsers::YYLTYPE &, Context * c)                  { context_stack.push(c); }
          void pop_context           (const All_Parsers::YYLTYPE &, Context * c)                  { while(context_stack.back()!=c) context_stack.pop(); }
          void emergency_pop_context (Context *)                                                  { while(context_stack.back()!=c) context_stack.pop(); }


          class Main_Context: public Context
          {
               bool check_if_legal(const All_Parsers::YYLTYPE &, const Operation *) const;
          };

          // Start building here:
          // --------------------

          Below_Prg_Builder(const std::string & filename);

          // Header:
          void set_nqubits  (const All_Parsers::YYLTYPE &, int);
          void set_ncregs   (const All_Parsers::YYLTYPE &, int);
          void append_crgsz (const All_Parsers::YYLTYPE &, int);
          void finish_cregs (const All_Parsers::YYLTYPE &  );
          void set_backend  (const All_Parsers::YYLTYPE &, const std::string &hwid);

          // Qubes:
     };

     using   Base::LQubit_t;
     using   Base::XY_t;
     using   Base::Coupling_tag;


     struct Operation
     {
          const All_Parsers::YYLTYPE * source_loc                               = nullptr;
          virtual std::string to_json() = 0;
          virtual std::string to_below_code() = 0;
     };

     // ----------------------------------
     // Qubes
     // ----------------------------------

     class Qube
     {
          friend class Qube_Builder;


          XY_t        dims;
          int         n_lqubits;

          enum Role_tag :int8_t { forbidden=-1, data=0, zero=1, borrowed=2 };
          std::vector< Role_tag >   input_role;                                    // size x*y -- use with Addr_t::lin()
          std::vector< Role_tag >   output_role;                                   // [ditto]

          std::vector< XY_t >  inputAddr_of_data;
          std::vector< XY_t >  inputAddr_of_borrow;
          std::vector< XY_t >  outputAddr_of_data;
          std::vector< XY_t >  outputAddr_of_borrow;

          // std::string unique_qube_id;                                           // All qubes with same functionality and same signature have same name...
          //                                                                       // ...but the unique qube id is unique.
          //                                                                       // Before mapping:  unique_qube_id == name+"["+var+"]".
          //                                                                       // After mapping (= file for hot phase): unique_qube_id == concise

          Operation_Sequence_t  op_seq;                                            // Opaque <=> `op_seq.empty()`

          // For Parsing:
          friend class Qube_Context;
          All_Parsers::YYLTYPE source_loc;

     public:
          const std::string & variant_id() const                                   { return variant;     }
          int                 n_lqubits()  const                                   { return n_lqubits;   }
          int                 xy_dims()    const                                   { return dims;        }
          float               time()       const                                   { return tm;          }

          std::vector<XY_t> get_inputAddr_of_role(Role_tag role);
          std::vector<XY_t> get_outputAddr_of_role(Role_tag role);
          std::string to_json();
          std::string to_below_code();

     }; //^ class Qube

     class Qube_Builder
     {
          Qube * p_the_qube;

     public:

          // CONTINUE HERE ......................................................................................................................................


          bool check_if_compatible(const All_Parsers::YYLTYPE &, const Qube & other) const;

          struct Qube_Context: public Below_Prg_Builder::Context
          {
               const Qube & Q;
               bool check_if_legal(const All_Parsers::YYLTYPE &, const Operation *) const;
          };
          Context* make_context() const { return new Qube_Context{*this}; }
          struct Qube_Context: public Below::Context { const Qube & Q; bool is_legal(const Operation &, std::string * const p_errmsg) const; };
          std::unique_ptr<Context> make_context() const { return new Qube_Context{*this}; }
     };


     // ----------------------------------
     // Qube Call Operation
     // ----------------------------------

     struct Qube_Call: public Operation
     {
          Qube const *              p_qube       = nullptr;                        // Pointer is managed somewhere else
          std::vector< LQubit_t >   operand;                                       // operands to the qube

          XY_t                      zero         = Void_Addr;                      // Phys location of zero corner (Void_Addr if not yet mapped)
          Base::Orientation         o            = Void_Orientation;               // Orientation of qube in world (Void_Orientation if not yet mapped)
          
          std::string to_json();
          std::string to_below_code();
     };

     // ----------------------------------
     // Elementary Operations                                                      (measurement, 1 qubit unitary, 2 qubit unitary)
     // ----------------------------------

     struct Elementary_Operation: public Operation {};                             // Base class for all elementary operations

     // struct Reset: public Elementary_Operation                                  // There's no reset operation. Use qufree+qualloc.



     struct Measurement: public Elementary_Operation
     {
          LQubit_t lqubit                                     = Void_LQubit;       // may be Void_LQubit only within qubes
          Addr_t   address                                    = Void_Addr;         // is Void_Addr before mapping

          CReg_with_Idx_t      creg_w_idx                     = Void_CReg_with_Idx;//
          
          std::string to_json();
          std::string to_below_code();
     };

     struct One_Qubit_Unitary: public Elementary_Operation
     {
          LQubit_t lqubit                                     = Void_LQubit;       // may be Void_LQubit only within qubes
          Addr_t   address                                    = Void_Addr;         // is Void_Addr before mapping

          Gates::One_Qubit_Unitary U;
          
          std::string to_json();
          std::string to_below_code();
     };

     struct Two_Qubit_Unitary: public Elementary_Operation
     {
          LQubit_t lqubit1                                    = Void_LQubit;       // may be Void_LQubit only within qubes
          LQubit_t lqubit2                                    = Void_LQubit;       // may be Void_LQubit only within qubes
          Addr_t   address1                                   = Void_Addr;         // is Void_Addr before mapping
          Addr_t   address2                                   = Void_Addr;         // is Void_Addr before mapping

          Two_Qubit_Productbasis_Unitary W;
          
          std::string to_json();
          std::string to_below_code();
     };

     // ------------------------------------------
     // Fridge Control Flow (If, Loop Operations)
     // ------------------------------------------

     struct FridgeControl_Operation: public Operation {};                          // base class for measurement-dependent control flow

     struct Fridge_Loop: public FridgeControl_Operation
     {
          Operation_Sequence_t loop_seq;

          Boolean_Expr_t       enter_cond;                                         // `if(enter_cond) do { loop_seq } while(stay_cond)`
          Boolean_Expr_t       stay_cond;

          unsigned             max_iter                       = MAX_UINT/2;
          
          std::string to_json();
          std::string to_below_code();
     };



     struct Fridge_IfThenElse: public Loop_Operation //TODO: @Dirk: Shouldn't it inherit from "FridgeControl_Operation" instead? Remove this comment if I'm wrong.
     {
          Operation_Sequence_t if_branch_seq;
          Operation_Sequence_t else_branch_seq;
          Boolean_Expr_t       if_cond;
          
          std::string to_json();
          std::string to_below_code();
     };

     // ----------------------------------
     // ColdControlFlow Operations
     // ----------------------------------

     struct ColdControlFlow_Operation: public Operation {};                                // base class 

     struct Cold_For_Loop: public ColdControlFlow_Operation
     {
          ...
     };

     struct Cold_IfThenElse: public ColdControlFlow_Operation
     {
          ...
     };

     // ----------------------------------
     // Special Operations
     // ----------------------------------

     struct Special_Operation: public Operation {};                                // base class 


     struct Rename: public Special_Operation
     {
          LQubit_t lqubit1                                    = Void_LQubit;       // may be Void_LQubit only within qubes
          LQubit_t lqubit2                                    = Void_LQubit;       // may be Void_LQubit only within qubes
     };

     struct Qualloc: public Special_Operation
     {
          LQubit_t lqubit                                     = Void_LQubit;       // may be Void_LQubit only within qubes
          Addr_t   address                                    = Void_Addr;         // is Void_Addr before mapping
     };

     struct Qufree: public Special_Operation
     {
          LQubit_t lqubit                                     = Void_LQubit;       // may be Void_LQubit only within qubes
          Addr_t   address                                    = Void_Addr;         // is Void_Addr before mapping
     };

     ////////////////////////////////////////////////////////////////////////////////////////////////////
     //
     // Stuff for the Below parser
     //

     struct compile_error: public std::exception
     {
          const std::string                 & msg;
          const All_Parsers::YYLTYPE        & loc;
          const std::unique_ptr<Source_loc>  p_loc2;

          virtual const char* what()  const noexcept                        { return msg.c_str(); }
          virtual Source_Loc  where() const noexcept                        { return loc;         }

          compile_error(const std::string & _msg,
                        const Source_Loc & _loc)                            : msg{_msg},loc{_loc},p_loc2(nullptr) {}
          compile_error(const std::string & _msg,
                        const Source_Loc & _loc1,const Source_Loc & _loc2)  : msg{_msg},loc{_loc},p_loc2{new Source_loc{_loc2}} {}
     };



}//^ namespace Below



#endif
//EOF
