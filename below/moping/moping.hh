// below/moping/moping.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 28, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This file contains definitions for the Mapping algorithms.
//
// To map your below program, do this:
//``   Moper_Function_t the_moper = available_mopers()["trivial"];
//``   (*the_moper)( &my_below_prg );
//
// To add a new mapping algorithm, do this:
//     1. Create a header file for it in this folder, say `below/moping/my_shiny_moper.hh`
//        That file should contain the declaration of function of type `moper_fn`.
//     2. Write the algorithm a .cc-file, `below/moping/my_shiny_moper.cc`.
//     3. In `moping.cc`, add a line to include the new algorithm: `#include "my_shiny_moper.hh"`
//     3. Also in `moping.cc`, add a line inserting your fn into the map:
//        `moper_container["my_shiny"] = &my_shiny_moper;`
//
//
// GNU C++17
//
#ifndef __BELOW_MOPING_HH__
#define __BELOW_MOPING_HH__

#include "../below_lang.hh"

namespace Below {

     namespace Moping {

          typedef std::map< std::string , double > Options_t;

          enum class Result_t { OK=0,
                    below_prg_malformed,
                    Ran_out_of_qubits,
                    Weird_error            };  // Todo: Expand


          typedef Result_t (*moper_fn)(Below_Prg *, const Options_t &);


          typedef std::map< std::string , moper_fn >  moper_container_t;

          const moper_container_t & available_mopers();

     } //^ namespace Moping
} //^ namespace Below

#endif
//EOF
