// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 20, 2018 by TVC
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// unicorn/unicorn_lang.cc
//
// This file contains the definitions of the Unicorn language.
//
// GNU C++17
//
#include "unicorn_lang.hh"

namespace Unicorn {
    
    Unicorn_Header::~Unicorn_Header() {
        //TODO: Fill in stuff
    }
    
    bool all_unitary(std::vector<Unicorn_Statement*> statements) {
        for (const Unicorn_Statement *statement : statements) {
            if(not (*statement).is_unitary()) {
                return false;
            }
        }
        return true;
    }
    
    Loop_Statement::~Loop_Statement() {
        for(const Unicorn_Statement *statement : body) {
            delete statement;
        }
        delete entry_condition;
        delete exit_condition;
    }
    
    bool Loop_Statement::is_unitary() {
        return all_unitary(body);
    }
    
    If_Then_Else_Statement::~If_Then_Else_Statement() {
        for(const Unicorn_Statement *statement : then_body) {
            delete statement;
        }
        for(const Unicorn_Statement *statement : else_body) {
            delete statement;
        }
        delete condition;
    }
    
    bool If_Then_Else_Statement::is_unitary() {
        if(all_unitary(then_body)) {
            return all_unitary(else_body);
        } else {
            return false;
        }
    }
    
    Proc_Call::~Proc_Call() {
        delete called_operation;
    }
    
    bool Proc_Call::validate() {
        return true;
    }
    
    Unitary_Call::~Unitary_Call() {
        delete called_operation;
    }
    
    bool Unitary_Call::validate() {
        if (not (*called_operation).is_unitary()) {
            return false;
        }
        if (not ctrled) {
            if (ctrling_qubit != Void_LQubit)
                return false;

        }
        return true;
    }
    
    bool Proc_Definition::validate() {
        return true;
    }
    
    bool Unitary_Definition::validate() {
        return all_unitary(code);
    }
    
    bool Unicorn_Prg::validate() {
        for (const Proc_Definition &proc_definition : proc_definitions) {
            if(not statement.validate()) {
                return false;
            }
        }
        return true;
    }
    
}//^ namespace Unicorn

//EOF
