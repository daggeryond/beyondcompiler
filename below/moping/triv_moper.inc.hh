// below/moping/triv_moper.inc.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 28, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This file is an include file with stuff (data structures, code) for DOT's trivial mapper.
//
// GNU C++17
//

using Base::LQubit_t;
using Base::XY_t;
using Base::Addr_t;
using Base::Orientation;
using Base::Physical_Unitary_1;
using Base::Physical_Productbasis_Unitary_2;
using Base::operator+=; // template
using Base::operator++; // template


// Ready, set, ...

// ___________________________________________________________________________________________________
//
// Classical bits in the fridge
// ___________________________________________________________________________________________________

enum CFridge_bit_idx : uint16_t { CFridge_bit_0=0, Void_CFridge_bit_idx=UINT16_MAX-3 };

template CFridge_bit_idx & operator+=(CFridge_bit_idx & x, CFridge_bit_idx d);
template CFridge_bit_idx & operator++(CFridge_bit_idx & x);

enum class CFridge_Access_tag { read, write };

template< CFridge_Access_tag t >
struct CFridge_Access
{
     const CFridge_bit_idx *sorted                                              = nullptr;  //   NULL  or  terminated by Void
};

typedef CFridge_Access<CFridge_Access_tag::read > CFridge_Read_Access_t;
typedef CFridge_Access<CFridge_Access_tag::write> CFridge_Write_Access_t;

template< CFridge_Access_tag t1, CFridge_Access_tag t2 > inline
bool
intersect(CFridge_Access<t1>, CFridge_Access<t2>);

inline
bool
conflict(CFridge_Read_Access_t, CFridge_Write_Access_t,                         // one operation
         CFridge_Read_Access_t, CFridge_Write_Access_t);                        // other operation



// ___________________________________________________________________________________________________
//
// Qubes with variants
// ___________________________________________________________________________________________________

struct alignas(0x40) Variant
{
     bool fits(Placement p)           const;
     bool coupling_fits(Placement p)  const;

private:
     CFridge_Read_Access_t    cfridge_read;
     CFridge_Write_Access_t   cfridge_write;

     XY_t *locations_in                                                         = nullptr; // Array size `n_data`+`n_borrow`+`n_zero`
                                                                                // First n_data are relative locations of the data qubits;
                                                                                // then n_borrow relative locations of the borrowed qubits;
                                                                                // then n_zero relative locations of (exchangeable) zero qubits.

     XY_t *locations_out                                                        = nullptr;
                                                                                // Array size `n_data`+`n_borrow` (zero qubits are exchangeable)

     float    time;

     XY_t anchor_pt;
     XY_t opposites_1, opposites_2;                                             // For `fits()`


     unsigned n_borrow                                                          = UINT_MAX-3;
     unsigned n_zero                                                            = UINT_MAX-3;
}; //^ struct Variant


template< typename BACKEND >
struct alignas(0x10) Lean_Qube
{
     using Backend_t = BACKEND;


private:

     LQubit_t * data_qubit_array;

     unsigned       variants_array__begin                                       = UNIT_MAX-3;  // for (unsigned v=variants_array__begin; v!=variants_array__begin+n_variants; ++v) do_stuff(variant[v]);
     unsigned short n_variants                                                  = USHRT_MAX-3;

     unsigned short n_data                                                      = USHRT_MAX-3;

}; //^ struct Lean_Qube

template< typename BACKEND >
struct Qube_Placement: private BACKEND
{
     using Base::Orientation_begin;
     using Base::Orientation_END;

     Addr_t      a                                                              = Base::Void_Addr;
     Orientation o                                                              = Base::Void_Orientation;

     Qube_Placement & operator++()                                              { ++o; if (o!=Orientation_END) return *this; o=Orientation_begin; ++a; return *this; }
     bool       operator!=(Placement i_) const                                  { return i_.a!=a || i_.o!=o; }



     // Non-member Friend functions

     friend
     Qube_Placement begin_placement()                                           { Placement p; p.a=p.begin(); p.o=Orientation_begin; return p; }

     friend
     Qube_Placement end_placement()                                             { Placement p; p.a=p.end(); p.o=Orientation_begin; return i;   }
};


template< typename BACKEND >
inline                                                                          // Whether the qube can be placed onto the backend with orientation and anchor-location given in `p`
bool can_place_here(const Lean_Qube<BACKEND> & qube,
                    Qube_Placement<BACKEND>       p)                            { return lq.fits(p) && lq.coupling_fits(p); }


template< typename BACKEND >
inline
bool overlap(const Lean_Qube<BACKEND> & qube1, Qube_Placement<BACKEND> p1,
             const Lean_Qube<BACKEND> & qube2, Qube_Placement<BACKEND> p2);

