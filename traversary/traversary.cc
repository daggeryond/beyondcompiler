// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 23, 2018 by TVC
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// traversary/traversary.cc
//
// This file contains basic concepts needed at high level.
//
// GNU C++17
//
#include "traversary.hh"

namespace Traversary {
    
    unique_ptr<Highlevel_Elementary_Operation> Explicit_Highlevel_Elementary_Operation::compute_dagger() {
        throw 42; //TODO: Implement correctly
    }
    
    vector<unique_ptr<Highlevel_Elementary_Operation>>* Explicit_Highlevel_Elementary_Operation::compute_ctrl() {
        retunr nullptr; //TODO: Implement correctly
    }
    
    unique_ptr<Highlevel_Elementary_Operation> Opague_Highlevel_Elementary_Operation::compute_dagger() {
        throw 42; //TODO: Implement correctly
    }
    
    vector<unique_ptr<Highlevel_Elementary_Operation>>* Opague_Highlevel_Elementary_Operation::compute_ctrl() {
        return nullptr; //TODO: Implement correctly
    }
    
} //^ namespace Traversary


