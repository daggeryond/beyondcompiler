// base/all_parsers.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This file contains the parser stuff that is needed by all modules.
//
// GNU C++17
//
#ifndef __BASE_ALL_PARSERS_HH__
#define __BASE_ALL_PARSERS_HH__


namespace Base {

     struct YYLTYPE
     {
          int first_line;                // default YYLLOC_DEFAULT still works.
          int first_column;
          int last_line;
          int last_column;

          const char * filename;
     };

}


#endif // EOF
