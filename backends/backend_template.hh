// backends/backend_template.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
//  MIT License
//
// Created July 6, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This is a template file for backends.
//
// GNU C++17
//
#ifndef __BACKENDS_UVWXYZ_HH__
#define __BACKENDS_UVWXYZ_HH__

namespace Backends {

     struct Uvwxyz                                                                        // CHANGE (must be unique in namespace Backends)
     {
          static constexpr Backend_ID_t id       {"string--arbitrary-length"};            // CHANGE (must be unique)
          static constexpr int          n_qubits = 43980465111040;                        // CHANGE
          static constexpr Coupling_tag coupling = Coupling_tag::grid_XOXO;               // CHANGE

          typedef Base::XY_t XY_t;


          // Decide if xy-coordinate is a qubit address:
          static inline
          bool inside(XY_t xy)               {  ...  }                                    // CHANGE



          // Iterating over all qubits
          // -------------------------

          struct Iter_t                /* Iterator type */                      { XY_t xy; XY_t operator*() const {return xy;} };  // Change or leave as it is

          constexpr Iter_t begin() const                                        { return Iter_t{XY_t{5, 0}}; }           // Change or leave as it is
          constexpr Iter_t end()   const                                        { return Iter_t{XY_t{6,11}}; }           // Change or leave as it is

     };

     inline // CHANGE class name, otherwise: Change or leave as it is
     Uvwxyz::Iter_t & operator ++ (Uvwxyz::Iter_t&i)                            { ++i.xy.x; if (!Bristlecone::inside(i.xy)) {++i.xy.y; i.xy.x=Base::X_t{(short)std::abs(i.xy.y-5)};} return i; }

     inline // CHANGE class name, otherwise: Change or leave as it is
     bool operator != (Uvwxyz::Iter_t&i,Uvwxyz::Iter_t&j)                       { return !( i.xy.x==j.xy.x && i.xy.y==j.xy.y ); }


     template<>
     inline // CHANGE class name
     float compute_time<Uvwxyz>(const Physical_Unitary_1 &)                     { ... } // CHANGE

     template<>
     inline // CHANGE class name
     std::string comile_to_asm<Uvwxyz>(const Physical_Unitary_1 &)              { ... } // CHANGE



     template<>
     inline // CHANGE class name
     float compute_time<Uvwxyz>(const Physical_Productbasis_Unitary_2 &)        { ... } // CHANGE

     template<>
     inline // CHANGE class name
     std::string compile_to_asm<Uvwxyz>(const Physical_Productbasis_Unitary_2&) { ... } // CHANGE



     template<>
     inline // CHANGE class name
     float compute_time_of_swap<Uvwxyz>()                                      { ... } // CHANGE

     template<>
     inline // CHANGE class name
     std::string compile_swap_to_asm<Uvwxyz>()                                 { ... } // CHANGE

} //^ namespace Backends

#endif // EOF
