/* This file is part of the Beyond project
|* (c) Ketita Labs 2018
|* MIT License
|/
|* Created May 25, 2018 by DOT
|* Contributors:
|*       Tore Vincent Carstens
|*       Bahman Ghandchi
|*       Dirk Oliver Theis
|**************************************************************************************
|*
|* This is the Flex file for the low-level intermediate representation, ``Below''.
|* This is genuine GNU Flex, incompatible with Lex.
*/

%{
#include "below_parser.tab.h"
%}

%option prefix="Below__yy"



HEX_DIGITS       0[xX][[:digit:][A-F]]+
BINARY_DIGITS    0[bB][01]+
FLOAT            ([+-]?(\.[:digit]+|[:digit:]+\.[:digit:]*)([eE][+-]?[:digit:]+)?)|([+-]?([:digit:]+)[eE][+-]?[:digit:]+)
XALPHA           [[:alpha:]_]

%%

HEX_DIGITS                                                                 {
                                                                               yylval.nneg_int = std::stoi(yytext+2,nullptr,16);
                                                                               return NNEG_INT;
                                                                           }
BINARY_DIGITS                                                              {
                                                                               yylval.nneg_int = std::stoi(yytext+2,nullptr,2);
                                                                               return NNEG_INT;
                                                                           }
[:digit:]+                                                                 {
                                                                               yylval.nneg_int = std::stoi(yytext,nullptr,0);
                                                                               return NNEG_INT;
                                                                           }


\"[[:alnum:][.:_]]+\"                                                      {
                                                                               yylval.string = yytext;
                                                                               yylval.string.pop_front();
                                                                               yylval.string.pop_back();
                                                                               return HARDWARE_ID;
                                                                           }


[[:alnum:][._]]+                                                           {
                                                                               yylval.string = yytext;
                                                                               return DATABASE_ID;
                                                                           }


"Qube"                                                                     {   BEGIN(expect_cplx_ID);
                                                                               return T_Qube;
                                                                           }

<expect_cplx_ID>XALPHA[[:alnum:][._<>]]*                                   {   BEGIN(0); // reset start condition
                                                                               yylval.string = yytext;
                                                                               return T_cplx_id;
                                                                           }

[:alpha:][[:alnum:]]*                                                      {
                                                                               yylval.string = yytext;
                                                                               return T_id;
                                                                           }


{FLOAT}                                                                    {
                                                                               yylval.dbl = std::stod(yytext);
                                                                               return ANGLE;
                                                                           }


"q#"{HEX_DIGITS}                                                           {
                                                                               yylval.nneg_int = std::stoi(yytext+4,nullptr,16);
                                                                               return LOGICAL_QUBIT;
                                                                           }
"q#"{BINARY_DIGITS}                                                        {
                                                                               yylval.nneg_int = std::stoi(yytext+4,nullptr,2);
                                                                               return LOGICAL_QUBIT;
                                                                           }
"q#"[:digit:]+                                                             {
                                                                               yylval.nneg_int = std::stoi(yytext+2,nullptr,0);
                                                                               return LOGICAL_QUBIT;
                                                                           }



"c#"{HEX_DIGITS}                                                           {
                                                                               yylval.nneg_int = std::stoi(yytext+4,nullptr,16);
                                                                               return CREG;
                                                                           }
"c#"{BINARY_DIGITS}                                                        {
                                                                               yylval.nneg_int = std::stoi(yytext+4,nullptr,2);
                                                                               return CREG;
                                                                           }
"c#"[:digit:]+                                                             {
                                                                               yylval.nneg_int = std::stoi(yytext+2,nullptr,0);
                                                                               return CREG;
                                                                           }


("p#"/\()|("@"/\()                                                         {
                                                                               return T_Address_Prefix;
                                                                           }


"begin_header"                                                             {
                                                                               return T_begin_header;
                                                                           }

"end_header"                                                               {
                                                                               return T_end_header;
                                                                           }

"nqubits"                                                                  {
                                                                               return T_nqubits;
                                                                           }

"ncregs"                                                                   {
                                                                               return T_ncregs;
                                                                           }

"creg_sizes"                                                               {
                                                                               return T_creg_sizes;
                                                                           }


"hardware_id"                                                              {
                                                                               return T_hardware_id;
                                                                           }

"begin_qubes"                                                              {
                                                                               return T_begin_qubes;
                                                                           }

"end_qubes"                                                                {
                                                                               return T_end_qubes;
                                                                           }

"Opaque"                                                                   {
                                                                               return T_Opaque;
                                                                           }

"begin_main"                                                               {
                                                                               return T_begin_main;
                                                                           }

"end_main"                                                                 {
                                                                               return T_end_main;
                                                                           }

"U3"                                                                       {
                                                                               return T_U3;
                                                                           }

"cU3"                                                                      {
                                                                               return T_cU3;
                                                                           }

"SWAP"                                                                     {
                                                                               return T_SWAP;
                                                                           }

"measure"                                                                  {
                                                                               return T_measure;
                                                                           }
"reset"                                                                    {
                                                                               return T_reset;
                                                                           }

"loop"                                                                     {
                                                                               return T_loop;
                                                                           }

"break_if"                                                                 {
                                                                               return T_break_if;
                                                                           }

"continue_if"                                                              {
                                                                               return T_cont_if;
                                                                           }

"if"                                                                       {
                                                                               return T_if;
                                                                           }

"while"                                                                    {
                                                                               return T_while;
                                                                           }

"qalloc"                                                                   {
                                                                               return T_qalloc;
                                                                           }

"qfree"                                                                    {
                                                                               return T_qfree;
                                                                           }

"exchange"                                                                 {
                                                                               return T_exchange;
                                                                           }

"data_in"                                                                  {
                                                                               return T_data_in;
                                                                           }
"data_out"                                                                 {
                                                                               return T_data_out;
                                                                           }

"borrows_in"                                                               {
                                                                               return T_borrows_in;
                                                                           }

"borrows_out"                                                              {
                                                                               return T_borrows_out;
                                                                           }

"zeros"                                                                    {
                                                                               return T_zeros;
                                                                           }


"->"                                                                       {
                                                                               return T_rightarrow;
                                                                           }

"=="                                                                       {
                                                                               return T_eqeq;
                                                                           }

[[:punct:][\(\)\[\]\{\}\<\>]]                                              {
                                                                               return yytext[0];
                                                                           }

%%
