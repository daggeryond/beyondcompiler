// backends/backends.hh
// *************************************************************************************************
// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created May 25, 2018 by DOT
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// This file contains the descriptions of the hardware backends.
//
// GNU C++17
//
#ifndef __BACKENDS_BACKENDS_HH__
#define __BACKENDS_BACKENDS_HH__

#include "../base/base.hh"

#include <string>

namespace Backends {

     using Base::Coupling_tag;

     struct Backend_ID_t
     {
          const char * const id;
     };
     constexpr Backend_ID_t Unrecognized_Backend {nullptr};


     extern   // Defined in  `backends.cc`
     Backend_ID_t id_backend(const std::string & id);    //  Returns ID or or `nullptr` if not recognized.


     template<typename BACKEND>
     inline
     float compute_time(const Physical_Unitary_1 &);

     template<typename BACKEND>
     inline
     std::string compile_to_asm(const Physical_Unitary_1 &);



     template<typename BACKEND>
     inline
     float compute_time(const Physical_Productbasis_Unitary_2 &);

     template<typename BACKEND>
     inline
     std::string compile_to_asm(const Physical_Productbasis_Unitary_2 &);



     template<typename BACKEND>
     inline
     float compute_time_of_swap();

     template<typename BACKEND>
     inline
     std::string compile_swap_to_asm();

}

#include "bristlecone.hh"


     ////////////////////////////////////////////////////////////////////////////////////////////////////
     //
     // IBM 50 Qubit Chip
     //

     struct IBM_Q50       {};


     ////////////////////////////////////////////////////////////////////////////////////////////////////
     //
     // "Pseudo"-Hardware
     //

     // Rigetti QPU resembling Acorn, except for the coupling, which is ``completed'' to a grid
     struct Pseudo_Acorn
     {
          static constexpr Backend_ID_t id       {"Rigetti_Pseudo_Acorn"};
          static constexpr int          n_qubits = 20;
          static constexpr Coupling_tag coupling = Coupling_tag::grid_O;

          // Gate set: R_z(θ), R_x(k π/2), Ctrl-Z, and Ctrl-Phase(θ)
     };


     // Imaginary Bristlecone 100x the size
     struct Imaginary_Bristlecone_x100 {};

    // grid with empty squares
     struct Generic_Grid          {};

    // chess-board pattern with X in bottom-right corner
     struct Generic_Chess_XO      {};

    // chess-board pattern with O in bottom-right corner
     struct Generic_Chess_OX      {};

    // grid with every square an X
     struct Generic_Grid_X        {};

} //^ namespace Backends

#endif // EOF
