// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 28, 2018 by TVC
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// ctrltree2below/ctrltree_to_below.hh
//
// This file contains the compilation from the ctrl-tree-format to the below-format.
//
// GNU C++17
//
#ifndef __CTRLTREETOBELOW_CTRLTREETOBELOW_HH__
#define __CTRLTREETOBELOW_CTRLTREETOBELOW_HH__

#include "../base/base.hh"
#include "../traversary/traversary.hh"
#include "../ctrltree/ctrltree_lang.hh"
#include "../below/below_lang.hh"

namespace Ctrltreetobelow {
    
    Locally_Flattened_Tree_Node* locally_flatten_tree_node(Tree_Node *tree_node);
    
    Locally_Flattened_Tree_Node* locally_flatten_ctrl_dagger_tree_node(LQubit_t ctrling_qubit, std::vector<Unitary_Tree_Node*>& ctrled_tree_nodes)
    
    Locally_Flattened_Tree_Node* locally_flatten_ctrl_ctrl_tree_node(LQubit_t ctrling_qubit, std::vector<Unitary_Tree_Node*>& ctrled_tree_nodes)
    
    Locally_Flattened_Tree_Node* locally_flatten_ctrl_composed_tree_node(LQubit_t ctrling_qubit, std::vector<Unitary_Tree_Node*>& ctrled_tree_nodes)
    
    Locally_Flattened_Tree_Node* locally_flatten_dagger_tree_node(Unitary_Tree_Node *daggered_tree_node);
    
    Locally_Flattened_Tree_Node* locally_flatten_ctrl_tree_node(LQubit_t ctrling_qubit, Unitary_Tree_Node *ctrled_tree_node);
    
    Locally_Flattened_Tree_Node* locally_flatten_composed_nonunitary_tree_node(std::vector<Tree_Node*>& tree_nodes);
    
    Locally_Flattened_Tree_Node* locally_flatten_composed_unitary_tree_node(std::vector<Unitary_Tree_Node*>& tree_nodes);
    
}//^ namespace Ctrltreetobelow


#endif
//EOF

