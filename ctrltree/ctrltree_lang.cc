// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 20, 2018 by TVC
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// ctrltree/ctrltree_lang.cc
//
// This file contains the definitions of the Ctrltree language.
//
// GNU C++17
//
#include "ctrltree_lang.hh"

namespace Ctrltree {
    
    Elementary_Operation::~Elementary_Operation() {
        delete unitary;
    }
    
    Composed_Nonunitary_Tree_Node::~Composed_Nonunitary_Tree_Node() {
        for(const Tree_Node *child : children) {
            delete child;
        }
    }
    
    ~Ctrl_Tree_Node() {
        delete ctrled_tree_node;
    }
    
    ~Dagger_Tree_Node() {
        delete daggered_tree_node;
    }
    
    Composed_Unitary_Tree_Node::~Composed_Unitary_Tree_Node() {
        for(const Unitary_Tree_Node *child : children) {
            delete child;
        }
    }
    
}//^ namespace Ctrltree

//EOF
