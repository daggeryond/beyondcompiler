// This file is part of the Beyond project
// (c) Ketita Labs 2018
// MIT License
//
// Created June 23, 2018 by TVC
// Contributors:
//       Tore Vincent Carstens
//       Bahman Ghandchi
//       Dirk Oliver Theis
////////////////////////////////////////////////////////////////////////////////////////////////////
//
// traversary/traversary.hh
//
// This file contains basic concepts needed at high level.
//
// GNU C++17
//
#ifndef __TRAVERSARY_TRAVERSARY_HH__
#define __TRAVERSARY_TRAVERSARY_HH__

#include "../base/base.hh"
#include "../base/gates.hh"

namespace Traversary {
    class Highlevel_Elementary_Operation {
        virtual unique_ptr<Highlevel_Elementary_Operation> compute_dagger() = 0;
        virtual unique_ptr<Highlevel_Elementary_Operation> compute_ctrl() = 0;
    }

    class Explicit_Highlevel_Elementary_Operation : public Highlevel_Elementary_Operation {
        unique_ptr<Elementary_Operation> operation;
        
        Explicit_Highlevel_Elementary_Operation(Elementary_Operation* operation_) operation{unique_ptr(operation_)} {}
        
        unique_ptr<Highlevel_Elementary_Operation> compute_dagger();
        
        vector<unique_ptr<Highlevel_Elementary_Operation>>* compute_ctrl();
    }
    
    class Opague_Highlevel_Elementary_Operation : public Highlevel_Elementary_Operation {
        std::string qube_name;
        
        inline
        Opague_Highlevel_Elementary_Operation(std::string qube_name_) qube_name{qube_name_} {}
        
        unique_ptr<Highlevel_Elementary_Operation> compute_dagger();
        
        vector<unique_ptr<Highlevel_Elementary_Operation>>* compute_ctrl();
    }
    
    class Highlevel_Controlflow_Condition {
        //TODO: Implement
    }
    
}//^ namespace Traversary


#endif
//EOF

